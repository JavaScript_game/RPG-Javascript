/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/create-character.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/character.js":
/*!*****************************!*\
  !*** ./src/js/character.js ***!
  \*****************************/
/*! exports provided: Character */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Character", function() { return Character; });
/* harmony import */ var _json_skills_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../json/skills.json */ "./src/json/skills.json");
var _json_skills_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../json/skills.json */ "./src/json/skills.json", 1);
/* harmony import */ var _json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../json/jobAttributs.json */ "./src/json/jobAttributs.json");
var _json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../json/jobAttributs.json */ "./src/json/jobAttributs.json", 1);




class Character {
/**
 * This class defines a Character. It takes different stats as parameters.
 * Different methods are used to interact with other Characters or with itself.
 * Theses methods manage the level up part, the stats growth and the profession choice.
 * maxHealth and maxMana stores the maximum amount of theses value so that heals don't overcap this stat.
 * @param {string} name 
 * @param {string} profession 
 * @param {number} health 
 * @param {number} mana 
 * @param {number} strength 
 * @param {number} vitality 
 * @param {number} magic 
 */
  constructor (name, profession, health, mana, strength, vitality, magic, exp = 0, level = 0){
    this.name = name;
    this.profession = profession;
    this.health = health;
    this.maxHealth = health;
    this.mana = mana;
    this.maxMana = mana;
    this.strength = strength;
    this.vitality = vitality;
    this.magic = magic;
    this.exp = exp;
    this.level = level;

    this.alive = true;

    this.ability1=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[profession][0].name;
    this.ability2=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[profession][1].name;
    this.ability3=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[profession][2].name;
    this.ability4="Heal";
  }

  /**
   * 
   * Takes in a value and adds it to the stat 
   */
  upgradeStrength(value){
    this.strength = this.strength + value
  }
  upgradeVitality(value){
    this.vitality = this.vitality + value
  }
  upgradeMagic(value){
    this.magic = this.magic + value
  }
  upgradeHealth(value){
    this.health = this.health + value
    this.maxHealth = this.health;
  }
  upgradeMana(value){
    this.mana = this.mana + value
    this.maxMana = this.mana;
  }

  healHealth(value){
    this.health += value;
    this.manageHealth();
  }
  healMana(value){
    this.mana += value;
    this.manageMana();
  }
  // Prevents health and mana to have values superior to max value
  manageHealth(){
    if (this.health > this.maxHealth){
      this.health = this.maxHealth;
    }
  }
  manageMana(){
    if (this.mana > this.maxMana){
      this.mana = this.maxMana;
    }
  }

  /**
   * 
   * Experience and level up.
   * Experience goes from 0 to 100, when hit hits 100 or more, exp recieves exp -100 and the   character gains a level (levelup)
   */
  gainExp (experiencePoints){
    this.exp = this.exp + experiencePoints;
    if (this.exp === 100){
      this.level = this.level++
      this.levelUp();
    }
    else if (this.exp > 100){
      this.levelUp();
      this.exp = this.exp - 100;
    }
  }
  levelUp (){       // HERE PUT ALL PROFESSION AND STATS GROWTH maxD ON THEM
    this.level++;
    if (this.profession === "Wizard"){
      this.upgradeStrength(1);
      this.upgradeVitality(2);
      this.upgradeMagic(5);
      this.upgradeHealth(10);
      this.upgradeMana(20);
    }
    else if (this.profession === "Fighter"){
      this.upgradeStrength(4);
      this.upgradeVitality(2);
      this.upgradeMagic(2);
      this.upgradeHealth(20);
      this.upgradeMana(10);
    }
    else if (this.profession === "Thief"){
      this.upgradeStrength(5);
      this.upgradeVitality(1);
      this.upgradeMagic(2);
      this.upgradeHealth(15);
      this.upgradeMana(15);
    }
    else if (this.profession === "Paladin"){
      this.upgradeStrength(3);
      this.upgradeVitality(4);
      this.upgradeMagic(2);
      this.upgradeHealth(16);
      this.upgradeMana(14);
    }
  }

  /**
   * strength and magic stats are used to deal damages to enemies
   */
  dealPhysicalDmg(){
    return this.strength;
  }
  dealMagicDmg(){
    return this.magic;
  }

  /**
   *  Methods to take in damages and check is the character is dead
   */
  takeDamage(value){
    this.health -= value;
  }

  isHeDead (){
    if (this.health <= 0){
      this.alive = false;
    }
  }
}

/***/ }),

/***/ "./src/js/create-character.js":
/*!************************************!*\
  !*** ./src/js/create-character.js ***!
  \************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./character */ "./src/js/character.js");
/* harmony import */ var _toolBox__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./toolBox */ "./src/js/toolBox.js");
/* harmony import */ var _json_skills_json__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../json/skills.json */ "./src/json/skills.json");
var _json_skills_json__WEBPACK_IMPORTED_MODULE_2___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../json/skills.json */ "./src/json/skills.json", 1);




/**
 * Theses variables get the name and profession of the main character (played character).
 * The stats of the character are defined by his profession.
 */
let name = document.querySelector('#name');
let profession = document.querySelector('#profession');
let mainCharacter = new _character__WEBPACK_IMPORTED_MODULE_0__["Character"]('Kiki', 'Fighter', 0, 0, 0, 0, 0); 

let tools = new _toolBox__WEBPACK_IMPORTED_MODULE_1__["Toolbox"](); // Contains numerous function.

/**
 * Here I assign the different stats to a variable and initialise them to a type = number
 */
let health = 0;
let mana = 0;
let strength = 0;
let vitality = 0;
let magic = 0;

/**
 * I target the different stats to different paragraph to display them as the player choses a profession
 */
let healthValue = document.querySelector('#job-health');
let manaValue = document.querySelector('#job-mana');
let strengthValue = document.querySelector('#job-strength');
let vitalityValue = document.querySelector('#job-vitality');
let magicValue = document.querySelector('#job-magic');
let option = document.querySelector('#profession');
let form = document.querySelector('#character-form');

option.addEventListener('change', function () {

  mainCharacter = tools.createCharacter(name.value, 
  profession.options[profession.selectedIndex].value);

  healthValue.textContent = `Health: ${mainCharacter.health}`;
  manaValue.textContent = `Mana: ${mainCharacter.mana}`;
  strengthValue.textContent = `Strength: ${mainCharacter.strength}`;
  vitalityValue.textContent = `Vitality: ${mainCharacter.vitality}`;
  magicValue.textContent = `Magic: ${mainCharacter.magic}`;

  console.log(mainCharacter);
});

let start = document.querySelector('#start');
start.addEventListener('click', function (event) {
  let characterStorage = JSON.stringify(mainCharacter)
  localStorage.setItem("mainCharacter", characterStorage);
});

/***/ }),

/***/ "./src/js/skill.js":
/*!*************************!*\
  !*** ./src/js/skill.js ***!
  \*************************/
/*! exports provided: Skill */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Skill", function() { return Skill; });
/* harmony import */ var _json_skills_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../json/skills.json */ "./src/json/skills.json");
var _json_skills_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../json/skills.json */ "./src/json/skills.json", 1);
 
class Skill {
  constructor(perso,action=1){
    this.waterDmg=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[perso.profession][action-1].water,
    this.fireDmg=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[perso.profession][action-1].fire,
    this.earthDmg=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[perso.profession][action-1].earth,
    this.shadowDmg=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[perso.profession][action-1].shadow,
    this.skillName=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[perso.profession][action-1].name
  }

  dealWaterDmg(bonus){
    let water=parseInt(this.waterDmg);
    return water*bonus
  }

  dealFireDmg(bonus){
    let fire=parseInt(this.fireDmg);
    return fire*bonus
  }

  dealEarthDmg(bonus){
   let  earth=parseInt(this.earthDmg);
    return earth*bonus
  }

  dealShadowDmg(bonus){
    let shadow=parseInt(this.shadowDmg);
    return shadow*bonus
  }

  dealAllDmg(bonus){
    return this.dealWaterDmg(bonus)+this.dealEarthDmg(bonus)+this.dealFireDmg(bonus)+this.dealShadowDmg(bonus)
  }
}

/***/ }),

/***/ "./src/js/toolBox.js":
/*!***************************!*\
  !*** ./src/js/toolBox.js ***!
  \***************************/
/*! exports provided: Toolbox */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Toolbox", function() { return Toolbox; });
/* harmony import */ var _character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./character */ "./src/js/character.js");
/* harmony import */ var _json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../json/jobAttributs.json */ "./src/json/jobAttributs.json");
var _json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../json/jobAttributs.json */ "./src/json/jobAttributs.json", 1);
/* harmony import */ var _skill__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./skill */ "./src/js/skill.js");




class Toolbox {
  constructor() {

  }
  createCharacter(name, profession) {

    if (profession === "Wizard") {
      let character = new _character__WEBPACK_IMPORTED_MODULE_0__["Character"](name, profession, 160, 200, 10, 10, 20);
      return character;
    }
    else if (profession === "Fighter") {
      let character = new _character__WEBPACK_IMPORTED_MODULE_0__["Character"](name, profession, 200, 100, 18, 12, 10);
      return character;
    }
    else if (profession === "Thief") {
      let character = new _character__WEBPACK_IMPORTED_MODULE_0__["Character"](name, profession, 180, 150, 20, 10, 10);
      return character;
    }
    else if (profession === "Paladin") {
      let character = new _character__WEBPACK_IMPORTED_MODULE_0__["Character"](name, profession, 190, 140, 13, 15, 12);
      return character;
    }
  }

  /**
 * 
 * @param {string} from the source of the damage
 * @param {string} to the char wounded
 * @param {number} dmg the number of damage
 */
  addLogCombat(from, to, dmg) {
    let logCombat = document.createElement('p');
    logCombat.textContent = `${to} takes ${dmg} damages from ${from}`;

    let logScroll = document.querySelector('#scroll');
    logScroll.prepend(logCombat);

    return dmg;
  }

  /**
 * 
 * @param {Character} character character object
 */
  computeBonus(character) {

    let randomizer = Math.random() * (1.5 - 1 + 1) + 1;


    if (character.profession === "Fighter") {

      let skill = Math.round(character.strength * randomizer);
      return skill / 10;
    }

    else if (character.profession === "Wizard") {

      let skill = Math.round(character.magic * randomizer);
      return skill / 10;
    }

    else if (character.profession === "Thief") {

      let skill = Math.round(character.strength + 5 * randomizer);
      return skill / 10;
    }

    else if (character.profession === "Paladin") {

      console.log(character.strength);

      let skill = Math.round((character.strength + character.magic) / 2 * randomizer)

      return (skill / 10);
    }

  }

  /**
 * Verifies if the fighters are alive. When one dies, his alive prop turn to false
 * @param {Character} player 
 * @param {Character} enemy 
 */
  isOver(enemy) {
    if (enemy.health <= 0) {
      enemy.alive = false;
      alert(`${enemy.name} is defeated. \n Congratulation!`);
      location.reload();
    }
  }

  /**
   * Launches a battle phase between two Characters. When the player does an action, the enemy replies after 1.2 seconds. Player can't play for 2 seconds
   * @param {Character} player 
   * @param {Character} enemy 
   * @param {Number} damage 
   */
  combatAction(player, enemy, damage = 0, heal = false) {
    let combatOver = false;
    //gestion barre de vie player
    let playerHealthBar = document.querySelector(`#player1 .bloc-health .health`);
    //gestion barre de vie enemy
    let enemyHealthBar = document.querySelector(`#enemy .bloc-health .health`);
    if (!heal) {
      enemy.health -= damage;
      enemy.manageHealth();
      this.addLogCombat(player.name, enemy.name, damage)

      if (enemy.health > 0) {
        enemyHealthBar.textContent = `${Math.floor((enemy.health / enemy.maxHealth) * 100)}%`
        enemyHealthBar.style.width = `${(enemy.health / enemy.maxHealth) * 100}%`
      }
      else {
        enemyHealthBar.textContent = `0%`
        enemyHealthBar.style.width = `0%`
        this.isOver(enemy);
      }

      console.log(`enemy health ${enemy.health} and ${damage} dmg`)
      if (enemy.health > 0) {
        setTimeout(() => {
          let enemyDamage = Math.round(enemy.magic * (Math.random() * (1.5 - 1 + 1) + 1));
          player.health -= enemyDamage ;

          player.manageHealth();

          if (player.health > 0) {
            playerHealthBar.textContent = `${Math.floor((player.health / player.maxHealth) * 100)}%`
            playerHealthBar.style.width = `${(player.health / player.maxHealth) * 100}%`
          }
          else {
            playerHealthBar.textContent = `0%`
            playerHealthBar.style.width = `0%`
            this.gameOver();
          }

          console.log(`Player health ${player.health} and ${enemy.magic}`)

          this.addLogCombat(enemy.name, player.name, enemyDamage)

        }, 1200);
      }
    }
    else {
      player.health += 22;
      player.manageHealth();
      //this.addLogCombat(this.addLogCombat(player.name, player.name, -22))

      if (player.health > 0) {
        playerHealthBar.textContent = `${Math.floor((player.health / player.maxHealth) * 100)}%`
        playerHealthBar.style.width = `${(player.health / player.maxHealth) * 100}%`
      }
      else {
        playerHealthBar.textContent = `0%`
        playerHealthBar.style.width = `0%`
        this.gameOver();
      }

      console.log(player.health);

      setTimeout(() => {
        enemy.health += 22;
        enemy.manageHealth();
        console.log(`enemy health ${enemy.health}`)

        if (enemy.health > 0) {
          enemyHealthBar.textContent = `${Math.floor((enemy.health / enemy.maxHealth) * 100)}%`
          enemyHealthBar.style.width = `${(enemy.health / enemy.maxHealth) * 100}%`
        }
        else {
          enemyHealthBar.textContent = `0%`
          enemyHealthBar.style.width = `0%`
          this.isOver(enemy);
        }

        this.addLogCombat(player.name, enemy.name, damage)

      }, 1200);
    }

  }

  gameOver() {
    if (confirm('You are dead. \n Click \"Ok\" to restart with this character.\n Click \"Annuler\" to creat a new character')) {
      location.reload();
    }
    else {
      location.href = "create-character.html";
    }
  }

  /**
 * 
 * @param {Character} player 
 * @param {boolean} canAttack 
 * @param {Character} enemy 
 * @param {number} buttonNumber 
 */
  combatView(player, canAttack, enemy, buttonNumber) {
    if (canAttack) {
      let skill = new _skill__WEBPACK_IMPORTED_MODULE_2__["Skill"](player, buttonNumber);
      let bonus = this.computeBonus(player)
      let dmg = skill.dealAllDmg(bonus);
      

      console.log(`Max health player : ${player.maxHealth}`)

      canAttack = false;
      this.combatAction(player, enemy, dmg);
      console.log(dmg)
      setTimeout(() => {
        canAttack = true;
      }, 1300);
      //console.log(` pdv actuel : ${player.health} de ${player.name}`)
      

      //console.log(` pdv actuel pre coup: ${enemy.health} de ${enemy.name}`)
      enemy.takeDamage(dmg);
      enemy.manageHealth();

      //console.log(` pdv actuel : ${enemy.health} de ${enemy.name}`)

      
    };
  };
}

/***/ }),

/***/ "./src/json/jobAttributs.json":
/*!************************************!*\
  !*** ./src/json/jobAttributs.json ***!
  \************************************/
/*! exports provided: Fighter, Wizard, Thief, Paladin, Ghost, Mecabird, Little Mage, default */
/***/ (function(module) {

module.exports = {"Fighter":[{"health":200,"mana":100,"strength":18,"vitality":12,"magic":10}],"Wizard":[{"health":100,"mana":200,"strength":10,"vitality":10,"magic":20}],"Thief":[{"health":150,"mana":150,"strength":20,"vitality":10,"magic":10}],"Paladin":[{"health":160,"mana":140,"strength":13,"vitality":15,"magic":12}],"Ghost":[{"name":"Ugly Casper","health":290,"mana":140,"strength":12,"vitality":15,"magic":23}],"Mecabird":[{"name":"Garurumon","health":250,"mana":140,"strength":12,"vitality":15,"magic":22}],"Little Mage":[{"name":"Jessica","health":310,"mana":140,"strength":13,"vitality":15,"magic":23}]};

/***/ }),

/***/ "./src/json/skills.json":
/*!******************************!*\
  !*** ./src/json/skills.json ***!
  \******************************/
/*! exports provided: Fighter, Thief, Wizard, Paladin, Little Mage, Mecabird, Ghost, default */
/***/ (function(module) {

module.exports = {"Fighter":[{"water":0,"fire":0,"earth":10,"shadow":0,"name":"Gaïa Clash"},{"water":0,"fire":10,"earth":0,"shadow":0,"name":"Fire Slash"},{"water":10,"fire":0,"earth":0,"shadow":0,"name":"Neptune hit"},{"water":10,"fire":10,"earth":0,"shadow":2,"name":"Heal sword"}],"Thief":[{"water":4,"fire":4,"earth":2,"shadow":0,"name":"Mug !"},{"water":2,"fire":4,"earth":4,"shadow":0,"name":"Back stab !"},{"water":0,"fire":0,"earth":0,"shadow":10,"name":"Slice in the night"},{"water":10,"fire":10,"earth":0,"shadow":2,"name":"Heal "}],"Wizard":[{"water":5,"fire":3,"earth":2,"shadow":0,"name":"Vanishing Bloom"},{"water":0,"fire":5,"earth":2,"shadow":2,"name":"Breath of dragoon"},{"water":5,"fire":0,"earth":0,"shadow":5,"name":"Valkyre of Poseidon"},{"water":10,"fire":10,"earth":0,"shadow":2,"name":"Neptune hit"}],"Paladin":[{"water":0,"fire":8,"earth":2,"shadow":0,"name":"Hammer of blossom"},{"water":0,"fire":4,"earth":4,"shadow":0,"name":"Hit by the light"},{"water":0,"fire":0,"earth":0,"shadow":10,"name":"Thunder Storm"},{"water":10,"fire":10,"earth":0,"shadow":2,"name":"Neptune hit"}],"Little Mage":[{"water":0,"fire":8,"earth":2,"shadow":0,"name":"Hammer of blossom"},{"water":0,"fire":4,"earth":4,"shadow":0,"name":"Hit by the light"},{"water":0,"fire":0,"earth":0,"shadow":10,"name":"Thunder Storm"},{"water":10,"fire":10,"earth":0,"shadow":2,"name":"Neptune hit"}],"Mecabird":[{"water":0,"fire":8,"earth":2,"shadow":0,"name":"Hammer of blossom"},{"water":0,"fire":4,"earth":4,"shadow":0,"name":"Hit by the light"},{"water":0,"fire":0,"earth":0,"shadow":10,"name":"Thunder Storm"}],"Ghost":[{"water":0,"fire":8,"earth":2,"shadow":0,"name":"Hammer of blossom"},{"water":0,"fire":4,"earth":4,"shadow":0,"name":"Hit by the light"},{"water":0,"fire":0,"earth":0,"shadow":10,"name":"Thunder Storm"}]};

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2NoYXJhY3Rlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvY3JlYXRlLWNoYXJhY3Rlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvc2tpbGwuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL3Rvb2xCb3guanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esa0RBQTBDLGdDQUFnQztBQUMxRTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGdFQUF3RCxrQkFBa0I7QUFDMUU7QUFDQSx5REFBaUQsY0FBYztBQUMvRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQXlDLGlDQUFpQztBQUMxRSx3SEFBZ0gsbUJBQW1CLEVBQUU7QUFDckk7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7O0FBR0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqRkE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdkpvQjtBQUNKO0FBQ2hCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtHOztBQUVBLG1FQUEwQjs7QUFFMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBLHVDQUF1QyxxQkFBcUI7QUFDNUQsbUNBQW1DLG1CQUFtQjtBQUN0RCwyQ0FBMkMsdUJBQXVCO0FBQ2xFLDJDQUEyQyx1QkFBdUI7QUFDbEUscUNBQXFDLG9CQUFvQjs7QUFFekQ7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQyxFOzs7Ozs7Ozs7Ozs7Ozs7O0FDcEREO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqQ29CO0FBQ3BCO0FBQ2M7O0FBRWQ7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBLCtCQUErQixHQUFHLFNBQVMsSUFBSSxnQkFBZ0IsS0FBSzs7QUFFcEU7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxXQUFXLFVBQVU7QUFDckI7QUFDQTs7QUFFQTs7O0FBR0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsV0FBVyxVQUFVO0FBQ3JCLFdBQVcsVUFBVTtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsV0FBVztBQUMxQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGFBQWEsVUFBVTtBQUN2QixhQUFhLFVBQVU7QUFDdkIsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSx3Q0FBd0MsbURBQW1EO0FBQzNGLHdDQUF3Qyx1Q0FBdUM7QUFDL0U7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGtDQUFrQyxhQUFhLE9BQU8sT0FBTztBQUM3RDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBLDZDQUE2QyxxREFBcUQ7QUFDbEcsNkNBQTZDLHlDQUF5QztBQUN0RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsdUNBQXVDLGNBQWMsT0FBTyxZQUFZOztBQUV4RTs7QUFFQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EseUNBQXlDLHFEQUFxRDtBQUM5Rix5Q0FBeUMseUNBQXlDO0FBQ2xGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxvQ0FBb0MsYUFBYTs7QUFFakQ7QUFDQSwwQ0FBMEMsbURBQW1EO0FBQzdGLDBDQUEwQyx1Q0FBdUM7QUFDakY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLE9BQU87QUFDUDs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxXQUFXLFVBQVU7QUFDckIsV0FBVyxRQUFRO0FBQ25CLFdBQVcsVUFBVTtBQUNyQixXQUFXLE9BQU87QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSx5Q0FBeUMsaUJBQWlCOztBQUUxRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLHFDQUFxQyxjQUFjLE1BQU0sWUFBWTs7O0FBR3JFLDZDQUE2QyxhQUFhLE1BQU0sV0FBVztBQUMzRTtBQUNBOztBQUVBLHFDQUFxQyxhQUFhLE1BQU0sV0FBVzs7O0FBR25FO0FBQ0E7QUFDQSxDIiwiZmlsZSI6Ii4vY3JlYXRfY2hhci5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9qcy9jcmVhdGUtY2hhcmFjdGVyLmpzXCIpO1xuIiwiXG5pbXBvcnQgc2tpbGxMaXN0IGZyb20gJy4uL2pzb24vc2tpbGxzLmpzb24nXG5pbXBvcnQgbW9uc3RlcnMgZnJvbSBcIi4uL2pzb24vam9iQXR0cmlidXRzLmpzb25cIlxuXG5leHBvcnQgY2xhc3MgQ2hhcmFjdGVyIHtcbi8qKlxuICogVGhpcyBjbGFzcyBkZWZpbmVzIGEgQ2hhcmFjdGVyLiBJdCB0YWtlcyBkaWZmZXJlbnQgc3RhdHMgYXMgcGFyYW1ldGVycy5cbiAqIERpZmZlcmVudCBtZXRob2RzIGFyZSB1c2VkIHRvIGludGVyYWN0IHdpdGggb3RoZXIgQ2hhcmFjdGVycyBvciB3aXRoIGl0c2VsZi5cbiAqIFRoZXNlcyBtZXRob2RzIG1hbmFnZSB0aGUgbGV2ZWwgdXAgcGFydCwgdGhlIHN0YXRzIGdyb3d0aCBhbmQgdGhlIHByb2Zlc3Npb24gY2hvaWNlLlxuICogbWF4SGVhbHRoIGFuZCBtYXhNYW5hIHN0b3JlcyB0aGUgbWF4aW11bSBhbW91bnQgb2YgdGhlc2VzIHZhbHVlIHNvIHRoYXQgaGVhbHMgZG9uJ3Qgb3ZlcmNhcCB0aGlzIHN0YXQuXG4gKiBAcGFyYW0ge3N0cmluZ30gbmFtZSBcbiAqIEBwYXJhbSB7c3RyaW5nfSBwcm9mZXNzaW9uIFxuICogQHBhcmFtIHtudW1iZXJ9IGhlYWx0aCBcbiAqIEBwYXJhbSB7bnVtYmVyfSBtYW5hIFxuICogQHBhcmFtIHtudW1iZXJ9IHN0cmVuZ3RoIFxuICogQHBhcmFtIHtudW1iZXJ9IHZpdGFsaXR5IFxuICogQHBhcmFtIHtudW1iZXJ9IG1hZ2ljIFxuICovXG4gIGNvbnN0cnVjdG9yIChuYW1lLCBwcm9mZXNzaW9uLCBoZWFsdGgsIG1hbmEsIHN0cmVuZ3RoLCB2aXRhbGl0eSwgbWFnaWMsIGV4cCA9IDAsIGxldmVsID0gMCl7XG4gICAgdGhpcy5uYW1lID0gbmFtZTtcbiAgICB0aGlzLnByb2Zlc3Npb24gPSBwcm9mZXNzaW9uO1xuICAgIHRoaXMuaGVhbHRoID0gaGVhbHRoO1xuICAgIHRoaXMubWF4SGVhbHRoID0gaGVhbHRoO1xuICAgIHRoaXMubWFuYSA9IG1hbmE7XG4gICAgdGhpcy5tYXhNYW5hID0gbWFuYTtcbiAgICB0aGlzLnN0cmVuZ3RoID0gc3RyZW5ndGg7XG4gICAgdGhpcy52aXRhbGl0eSA9IHZpdGFsaXR5O1xuICAgIHRoaXMubWFnaWMgPSBtYWdpYztcbiAgICB0aGlzLmV4cCA9IGV4cDtcbiAgICB0aGlzLmxldmVsID0gbGV2ZWw7XG5cbiAgICB0aGlzLmFsaXZlID0gdHJ1ZTtcblxuICAgIHRoaXMuYWJpbGl0eTE9c2tpbGxMaXN0W3Byb2Zlc3Npb25dWzBdLm5hbWU7XG4gICAgdGhpcy5hYmlsaXR5Mj1za2lsbExpc3RbcHJvZmVzc2lvbl1bMV0ubmFtZTtcbiAgICB0aGlzLmFiaWxpdHkzPXNraWxsTGlzdFtwcm9mZXNzaW9uXVsyXS5uYW1lO1xuICAgIHRoaXMuYWJpbGl0eTQ9XCJIZWFsXCI7XG4gIH1cblxuICAvKipcbiAgICogXG4gICAqIFRha2VzIGluIGEgdmFsdWUgYW5kIGFkZHMgaXQgdG8gdGhlIHN0YXQgXG4gICAqL1xuICB1cGdyYWRlU3RyZW5ndGgodmFsdWUpe1xuICAgIHRoaXMuc3RyZW5ndGggPSB0aGlzLnN0cmVuZ3RoICsgdmFsdWVcbiAgfVxuICB1cGdyYWRlVml0YWxpdHkodmFsdWUpe1xuICAgIHRoaXMudml0YWxpdHkgPSB0aGlzLnZpdGFsaXR5ICsgdmFsdWVcbiAgfVxuICB1cGdyYWRlTWFnaWModmFsdWUpe1xuICAgIHRoaXMubWFnaWMgPSB0aGlzLm1hZ2ljICsgdmFsdWVcbiAgfVxuICB1cGdyYWRlSGVhbHRoKHZhbHVlKXtcbiAgICB0aGlzLmhlYWx0aCA9IHRoaXMuaGVhbHRoICsgdmFsdWVcbiAgICB0aGlzLm1heEhlYWx0aCA9IHRoaXMuaGVhbHRoO1xuICB9XG4gIHVwZ3JhZGVNYW5hKHZhbHVlKXtcbiAgICB0aGlzLm1hbmEgPSB0aGlzLm1hbmEgKyB2YWx1ZVxuICAgIHRoaXMubWF4TWFuYSA9IHRoaXMubWFuYTtcbiAgfVxuXG4gIGhlYWxIZWFsdGgodmFsdWUpe1xuICAgIHRoaXMuaGVhbHRoICs9IHZhbHVlO1xuICAgIHRoaXMubWFuYWdlSGVhbHRoKCk7XG4gIH1cbiAgaGVhbE1hbmEodmFsdWUpe1xuICAgIHRoaXMubWFuYSArPSB2YWx1ZTtcbiAgICB0aGlzLm1hbmFnZU1hbmEoKTtcbiAgfVxuICAvLyBQcmV2ZW50cyBoZWFsdGggYW5kIG1hbmEgdG8gaGF2ZSB2YWx1ZXMgc3VwZXJpb3IgdG8gbWF4IHZhbHVlXG4gIG1hbmFnZUhlYWx0aCgpe1xuICAgIGlmICh0aGlzLmhlYWx0aCA+IHRoaXMubWF4SGVhbHRoKXtcbiAgICAgIHRoaXMuaGVhbHRoID0gdGhpcy5tYXhIZWFsdGg7XG4gICAgfVxuICB9XG4gIG1hbmFnZU1hbmEoKXtcbiAgICBpZiAodGhpcy5tYW5hID4gdGhpcy5tYXhNYW5hKXtcbiAgICAgIHRoaXMubWFuYSA9IHRoaXMubWF4TWFuYTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogXG4gICAqIEV4cGVyaWVuY2UgYW5kIGxldmVsIHVwLlxuICAgKiBFeHBlcmllbmNlIGdvZXMgZnJvbSAwIHRvIDEwMCwgd2hlbiBoaXQgaGl0cyAxMDAgb3IgbW9yZSwgZXhwIHJlY2lldmVzIGV4cCAtMTAwIGFuZCB0aGUgICBjaGFyYWN0ZXIgZ2FpbnMgYSBsZXZlbCAobGV2ZWx1cClcbiAgICovXG4gIGdhaW5FeHAgKGV4cGVyaWVuY2VQb2ludHMpe1xuICAgIHRoaXMuZXhwID0gdGhpcy5leHAgKyBleHBlcmllbmNlUG9pbnRzO1xuICAgIGlmICh0aGlzLmV4cCA9PT0gMTAwKXtcbiAgICAgIHRoaXMubGV2ZWwgPSB0aGlzLmxldmVsKytcbiAgICAgIHRoaXMubGV2ZWxVcCgpO1xuICAgIH1cbiAgICBlbHNlIGlmICh0aGlzLmV4cCA+IDEwMCl7XG4gICAgICB0aGlzLmxldmVsVXAoKTtcbiAgICAgIHRoaXMuZXhwID0gdGhpcy5leHAgLSAxMDA7XG4gICAgfVxuICB9XG4gIGxldmVsVXAgKCl7ICAgICAgIC8vIEhFUkUgUFVUIEFMTCBQUk9GRVNTSU9OIEFORCBTVEFUUyBHUk9XVEggbWF4RCBPTiBUSEVNXG4gICAgdGhpcy5sZXZlbCsrO1xuICAgIGlmICh0aGlzLnByb2Zlc3Npb24gPT09IFwiV2l6YXJkXCIpe1xuICAgICAgdGhpcy51cGdyYWRlU3RyZW5ndGgoMSk7XG4gICAgICB0aGlzLnVwZ3JhZGVWaXRhbGl0eSgyKTtcbiAgICAgIHRoaXMudXBncmFkZU1hZ2ljKDUpO1xuICAgICAgdGhpcy51cGdyYWRlSGVhbHRoKDEwKTtcbiAgICAgIHRoaXMudXBncmFkZU1hbmEoMjApO1xuICAgIH1cbiAgICBlbHNlIGlmICh0aGlzLnByb2Zlc3Npb24gPT09IFwiRmlnaHRlclwiKXtcbiAgICAgIHRoaXMudXBncmFkZVN0cmVuZ3RoKDQpO1xuICAgICAgdGhpcy51cGdyYWRlVml0YWxpdHkoMik7XG4gICAgICB0aGlzLnVwZ3JhZGVNYWdpYygyKTtcbiAgICAgIHRoaXMudXBncmFkZUhlYWx0aCgyMCk7XG4gICAgICB0aGlzLnVwZ3JhZGVNYW5hKDEwKTtcbiAgICB9XG4gICAgZWxzZSBpZiAodGhpcy5wcm9mZXNzaW9uID09PSBcIlRoaWVmXCIpe1xuICAgICAgdGhpcy51cGdyYWRlU3RyZW5ndGgoNSk7XG4gICAgICB0aGlzLnVwZ3JhZGVWaXRhbGl0eSgxKTtcbiAgICAgIHRoaXMudXBncmFkZU1hZ2ljKDIpO1xuICAgICAgdGhpcy51cGdyYWRlSGVhbHRoKDE1KTtcbiAgICAgIHRoaXMudXBncmFkZU1hbmEoMTUpO1xuICAgIH1cbiAgICBlbHNlIGlmICh0aGlzLnByb2Zlc3Npb24gPT09IFwiUGFsYWRpblwiKXtcbiAgICAgIHRoaXMudXBncmFkZVN0cmVuZ3RoKDMpO1xuICAgICAgdGhpcy51cGdyYWRlVml0YWxpdHkoNCk7XG4gICAgICB0aGlzLnVwZ3JhZGVNYWdpYygyKTtcbiAgICAgIHRoaXMudXBncmFkZUhlYWx0aCgxNik7XG4gICAgICB0aGlzLnVwZ3JhZGVNYW5hKDE0KTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogc3RyZW5ndGggYW5kIG1hZ2ljIHN0YXRzIGFyZSB1c2VkIHRvIGRlYWwgZGFtYWdlcyB0byBlbmVtaWVzXG4gICAqL1xuICBkZWFsUGh5c2ljYWxEbWcoKXtcbiAgICByZXR1cm4gdGhpcy5zdHJlbmd0aDtcbiAgfVxuICBkZWFsTWFnaWNEbWcoKXtcbiAgICByZXR1cm4gdGhpcy5tYWdpYztcbiAgfVxuXG4gIC8qKlxuICAgKiAgTWV0aG9kcyB0byB0YWtlIGluIGRhbWFnZXMgYW5kIGNoZWNrIGlzIHRoZSBjaGFyYWN0ZXIgaXMgZGVhZFxuICAgKi9cbiAgdGFrZURhbWFnZSh2YWx1ZSl7XG4gICAgdGhpcy5oZWFsdGggLT0gdmFsdWU7XG4gIH1cblxuICBpc0hlRGVhZCAoKXtcbiAgICBpZiAodGhpcy5oZWFsdGggPD0gMCl7XG4gICAgICB0aGlzLmFsaXZlID0gZmFsc2U7XG4gICAgfVxuICB9XG59IiwiaW1wb3J0IHsgQ2hhcmFjdGVyIH0gZnJvbSAnLi9jaGFyYWN0ZXInO1xuaW1wb3J0IHtUb29sYm94fSBmcm9tICcuL3Rvb2xCb3gnO1xuaW1wb3J0IHNraWxsTGlzdCBmcm9tICcuLi9qc29uL3NraWxscy5qc29uJ1xuXG4vKipcbiAqIFRoZXNlcyB2YXJpYWJsZXMgZ2V0IHRoZSBuYW1lIGFuZCBwcm9mZXNzaW9uIG9mIHRoZSBtYWluIGNoYXJhY3RlciAocGxheWVkIGNoYXJhY3RlcikuXG4gKiBUaGUgc3RhdHMgb2YgdGhlIGNoYXJhY3RlciBhcmUgZGVmaW5lZCBieSBoaXMgcHJvZmVzc2lvbi5cbiAqL1xubGV0IG5hbWUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjbmFtZScpO1xubGV0IHByb2Zlc3Npb24gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjcHJvZmVzc2lvbicpO1xubGV0IG1haW5DaGFyYWN0ZXIgPSBuZXcgQ2hhcmFjdGVyKCdLaWtpJywgJ0ZpZ2h0ZXInLCAwLCAwLCAwLCAwLCAwKTsgXG5cbmxldCB0b29scyA9IG5ldyBUb29sYm94KCk7IC8vIENvbnRhaW5zIG51bWVyb3VzIGZ1bmN0aW9uLlxuXG4vKipcbiAqIEhlcmUgSSBhc3NpZ24gdGhlIGRpZmZlcmVudCBzdGF0cyB0byBhIHZhcmlhYmxlIGFuZCBpbml0aWFsaXNlIHRoZW0gdG8gYSB0eXBlID0gbnVtYmVyXG4gKi9cbmxldCBoZWFsdGggPSAwO1xubGV0IG1hbmEgPSAwO1xubGV0IHN0cmVuZ3RoID0gMDtcbmxldCB2aXRhbGl0eSA9IDA7XG5sZXQgbWFnaWMgPSAwO1xuXG4vKipcbiAqIEkgdGFyZ2V0IHRoZSBkaWZmZXJlbnQgc3RhdHMgdG8gZGlmZmVyZW50IHBhcmFncmFwaCB0byBkaXNwbGF5IHRoZW0gYXMgdGhlIHBsYXllciBjaG9zZXMgYSBwcm9mZXNzaW9uXG4gKi9cbmxldCBoZWFsdGhWYWx1ZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNqb2ItaGVhbHRoJyk7XG5sZXQgbWFuYVZhbHVlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2pvYi1tYW5hJyk7XG5sZXQgc3RyZW5ndGhWYWx1ZSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNqb2Itc3RyZW5ndGgnKTtcbmxldCB2aXRhbGl0eVZhbHVlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2pvYi12aXRhbGl0eScpO1xubGV0IG1hZ2ljVmFsdWUgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjam9iLW1hZ2ljJyk7XG5sZXQgb3B0aW9uID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI3Byb2Zlc3Npb24nKTtcbmxldCBmb3JtID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignI2NoYXJhY3Rlci1mb3JtJyk7XG5cbm9wdGlvbi5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCBmdW5jdGlvbiAoKSB7XG5cbiAgbWFpbkNoYXJhY3RlciA9IHRvb2xzLmNyZWF0ZUNoYXJhY3RlcihuYW1lLnZhbHVlLCBcbiAgcHJvZmVzc2lvbi5vcHRpb25zW3Byb2Zlc3Npb24uc2VsZWN0ZWRJbmRleF0udmFsdWUpO1xuXG4gIGhlYWx0aFZhbHVlLnRleHRDb250ZW50ID0gYEhlYWx0aDogJHttYWluQ2hhcmFjdGVyLmhlYWx0aH1gO1xuICBtYW5hVmFsdWUudGV4dENvbnRlbnQgPSBgTWFuYTogJHttYWluQ2hhcmFjdGVyLm1hbmF9YDtcbiAgc3RyZW5ndGhWYWx1ZS50ZXh0Q29udGVudCA9IGBTdHJlbmd0aDogJHttYWluQ2hhcmFjdGVyLnN0cmVuZ3RofWA7XG4gIHZpdGFsaXR5VmFsdWUudGV4dENvbnRlbnQgPSBgVml0YWxpdHk6ICR7bWFpbkNoYXJhY3Rlci52aXRhbGl0eX1gO1xuICBtYWdpY1ZhbHVlLnRleHRDb250ZW50ID0gYE1hZ2ljOiAke21haW5DaGFyYWN0ZXIubWFnaWN9YDtcblxuICBjb25zb2xlLmxvZyhtYWluQ2hhcmFjdGVyKTtcbn0pO1xuXG5sZXQgc3RhcnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjc3RhcnQnKTtcbnN0YXJ0LmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKGV2ZW50KSB7XG4gIGxldCBjaGFyYWN0ZXJTdG9yYWdlID0gSlNPTi5zdHJpbmdpZnkobWFpbkNoYXJhY3RlcilcbiAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJtYWluQ2hhcmFjdGVyXCIsIGNoYXJhY3RlclN0b3JhZ2UpO1xufSk7IiwiaW1wb3J0IHNraWxsc2V0IGZyb20gJy4uL2pzb24vc2tpbGxzLmpzb24nIFxuZXhwb3J0IGNsYXNzIFNraWxsIHtcbiAgY29uc3RydWN0b3IocGVyc28sYWN0aW9uPTEpe1xuICAgIHRoaXMud2F0ZXJEbWc9c2tpbGxzZXRbcGVyc28ucHJvZmVzc2lvbl1bYWN0aW9uLTFdLndhdGVyLFxuICAgIHRoaXMuZmlyZURtZz1za2lsbHNldFtwZXJzby5wcm9mZXNzaW9uXVthY3Rpb24tMV0uZmlyZSxcbiAgICB0aGlzLmVhcnRoRG1nPXNraWxsc2V0W3BlcnNvLnByb2Zlc3Npb25dW2FjdGlvbi0xXS5lYXJ0aCxcbiAgICB0aGlzLnNoYWRvd0RtZz1za2lsbHNldFtwZXJzby5wcm9mZXNzaW9uXVthY3Rpb24tMV0uc2hhZG93LFxuICAgIHRoaXMuc2tpbGxOYW1lPXNraWxsc2V0W3BlcnNvLnByb2Zlc3Npb25dW2FjdGlvbi0xXS5uYW1lXG4gIH1cblxuICBkZWFsV2F0ZXJEbWcoYm9udXMpe1xuICAgIGxldCB3YXRlcj1wYXJzZUludCh0aGlzLndhdGVyRG1nKTtcbiAgICByZXR1cm4gd2F0ZXIqYm9udXNcbiAgfVxuXG4gIGRlYWxGaXJlRG1nKGJvbnVzKXtcbiAgICBsZXQgZmlyZT1wYXJzZUludCh0aGlzLmZpcmVEbWcpO1xuICAgIHJldHVybiBmaXJlKmJvbnVzXG4gIH1cblxuICBkZWFsRWFydGhEbWcoYm9udXMpe1xuICAgbGV0ICBlYXJ0aD1wYXJzZUludCh0aGlzLmVhcnRoRG1nKTtcbiAgICByZXR1cm4gZWFydGgqYm9udXNcbiAgfVxuXG4gIGRlYWxTaGFkb3dEbWcoYm9udXMpe1xuICAgIGxldCBzaGFkb3c9cGFyc2VJbnQodGhpcy5zaGFkb3dEbWcpO1xuICAgIHJldHVybiBzaGFkb3cqYm9udXNcbiAgfVxuXG4gIGRlYWxBbGxEbWcoYm9udXMpe1xuICAgIHJldHVybiB0aGlzLmRlYWxXYXRlckRtZyhib251cykrdGhpcy5kZWFsRWFydGhEbWcoYm9udXMpK3RoaXMuZGVhbEZpcmVEbWcoYm9udXMpK3RoaXMuZGVhbFNoYWRvd0RtZyhib251cylcbiAgfVxufSIsImltcG9ydCB7IENoYXJhY3RlciB9IGZyb20gJy4vY2hhcmFjdGVyJztcbmltcG9ydCBDaGFyYWN0ZXJBdHRyaWJ1dHMgZnJvbSAnLi4vanNvbi9qb2JBdHRyaWJ1dHMuanNvbidcbmltcG9ydCB7U2tpbGx9IGZyb20gJy4vc2tpbGwnXG5cbmV4cG9ydCBjbGFzcyBUb29sYm94IHtcbiAgY29uc3RydWN0b3IoKSB7XG5cbiAgfVxuICBjcmVhdGVDaGFyYWN0ZXIobmFtZSwgcHJvZmVzc2lvbikge1xuXG4gICAgaWYgKHByb2Zlc3Npb24gPT09IFwiV2l6YXJkXCIpIHtcbiAgICAgIGxldCBjaGFyYWN0ZXIgPSBuZXcgQ2hhcmFjdGVyKG5hbWUsIHByb2Zlc3Npb24sIDE2MCwgMjAwLCAxMCwgMTAsIDIwKTtcbiAgICAgIHJldHVybiBjaGFyYWN0ZXI7XG4gICAgfVxuICAgIGVsc2UgaWYgKHByb2Zlc3Npb24gPT09IFwiRmlnaHRlclwiKSB7XG4gICAgICBsZXQgY2hhcmFjdGVyID0gbmV3IENoYXJhY3RlcihuYW1lLCBwcm9mZXNzaW9uLCAyMDAsIDEwMCwgMTgsIDEyLCAxMCk7XG4gICAgICByZXR1cm4gY2hhcmFjdGVyO1xuICAgIH1cbiAgICBlbHNlIGlmIChwcm9mZXNzaW9uID09PSBcIlRoaWVmXCIpIHtcbiAgICAgIGxldCBjaGFyYWN0ZXIgPSBuZXcgQ2hhcmFjdGVyKG5hbWUsIHByb2Zlc3Npb24sIDE4MCwgMTUwLCAyMCwgMTAsIDEwKTtcbiAgICAgIHJldHVybiBjaGFyYWN0ZXI7XG4gICAgfVxuICAgIGVsc2UgaWYgKHByb2Zlc3Npb24gPT09IFwiUGFsYWRpblwiKSB7XG4gICAgICBsZXQgY2hhcmFjdGVyID0gbmV3IENoYXJhY3RlcihuYW1lLCBwcm9mZXNzaW9uLCAxOTAsIDE0MCwgMTMsIDE1LCAxMik7XG4gICAgICByZXR1cm4gY2hhcmFjdGVyO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICogXG4gKiBAcGFyYW0ge3N0cmluZ30gZnJvbSB0aGUgc291cmNlIG9mIHRoZSBkYW1hZ2VcbiAqIEBwYXJhbSB7c3RyaW5nfSB0byB0aGUgY2hhciB3b3VuZGVkXG4gKiBAcGFyYW0ge251bWJlcn0gZG1nIHRoZSBudW1iZXIgb2YgZGFtYWdlXG4gKi9cbiAgYWRkTG9nQ29tYmF0KGZyb20sIHRvLCBkbWcpIHtcbiAgICBsZXQgbG9nQ29tYmF0ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgncCcpO1xuICAgIGxvZ0NvbWJhdC50ZXh0Q29udGVudCA9IGAke3RvfSB0YWtlcyAke2RtZ30gZGFtYWdlcyBmcm9tICR7ZnJvbX1gO1xuXG4gICAgbGV0IGxvZ1Njcm9sbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNzY3JvbGwnKTtcbiAgICBsb2dTY3JvbGwucHJlcGVuZChsb2dDb21iYXQpO1xuXG4gICAgcmV0dXJuIGRtZztcbiAgfVxuXG4gIC8qKlxuICogXG4gKiBAcGFyYW0ge0NoYXJhY3Rlcn0gY2hhcmFjdGVyIGNoYXJhY3RlciBvYmplY3RcbiAqL1xuICBjb21wdXRlQm9udXMoY2hhcmFjdGVyKSB7XG5cbiAgICBsZXQgcmFuZG9taXplciA9IE1hdGgucmFuZG9tKCkgKiAoMS41IC0gMSArIDEpICsgMTtcblxuXG4gICAgaWYgKGNoYXJhY3Rlci5wcm9mZXNzaW9uID09PSBcIkZpZ2h0ZXJcIikge1xuXG4gICAgICBsZXQgc2tpbGwgPSBNYXRoLnJvdW5kKGNoYXJhY3Rlci5zdHJlbmd0aCAqIHJhbmRvbWl6ZXIpO1xuICAgICAgcmV0dXJuIHNraWxsIC8gMTA7XG4gICAgfVxuXG4gICAgZWxzZSBpZiAoY2hhcmFjdGVyLnByb2Zlc3Npb24gPT09IFwiV2l6YXJkXCIpIHtcblxuICAgICAgbGV0IHNraWxsID0gTWF0aC5yb3VuZChjaGFyYWN0ZXIubWFnaWMgKiByYW5kb21pemVyKTtcbiAgICAgIHJldHVybiBza2lsbCAvIDEwO1xuICAgIH1cblxuICAgIGVsc2UgaWYgKGNoYXJhY3Rlci5wcm9mZXNzaW9uID09PSBcIlRoaWVmXCIpIHtcblxuICAgICAgbGV0IHNraWxsID0gTWF0aC5yb3VuZChjaGFyYWN0ZXIuc3RyZW5ndGggKyA1ICogcmFuZG9taXplcik7XG4gICAgICByZXR1cm4gc2tpbGwgLyAxMDtcbiAgICB9XG5cbiAgICBlbHNlIGlmIChjaGFyYWN0ZXIucHJvZmVzc2lvbiA9PT0gXCJQYWxhZGluXCIpIHtcblxuICAgICAgY29uc29sZS5sb2coY2hhcmFjdGVyLnN0cmVuZ3RoKTtcblxuICAgICAgbGV0IHNraWxsID0gTWF0aC5yb3VuZCgoY2hhcmFjdGVyLnN0cmVuZ3RoICsgY2hhcmFjdGVyLm1hZ2ljKSAvIDIgKiByYW5kb21pemVyKVxuXG4gICAgICByZXR1cm4gKHNraWxsIC8gMTApO1xuICAgIH1cblxuICB9XG5cbiAgLyoqXG4gKiBWZXJpZmllcyBpZiB0aGUgZmlnaHRlcnMgYXJlIGFsaXZlLiBXaGVuIG9uZSBkaWVzLCBoaXMgYWxpdmUgcHJvcCB0dXJuIHRvIGZhbHNlXG4gKiBAcGFyYW0ge0NoYXJhY3Rlcn0gcGxheWVyIFxuICogQHBhcmFtIHtDaGFyYWN0ZXJ9IGVuZW15IFxuICovXG4gIGlzT3ZlcihlbmVteSkge1xuICAgIGlmIChlbmVteS5oZWFsdGggPD0gMCkge1xuICAgICAgZW5lbXkuYWxpdmUgPSBmYWxzZTtcbiAgICAgIGFsZXJ0KGAke2VuZW15Lm5hbWV9IGlzIGRlZmVhdGVkLiBcXG4gQ29uZ3JhdHVsYXRpb24hYCk7XG4gICAgICBsb2NhdGlvbi5yZWxvYWQoKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogTGF1bmNoZXMgYSBiYXR0bGUgcGhhc2UgYmV0d2VlbiB0d28gQ2hhcmFjdGVycy4gV2hlbiB0aGUgcGxheWVyIGRvZXMgYW4gYWN0aW9uLCB0aGUgZW5lbXkgcmVwbGllcyBhZnRlciAxLjIgc2Vjb25kcy4gUGxheWVyIGNhbid0IHBsYXkgZm9yIDIgc2Vjb25kc1xuICAgKiBAcGFyYW0ge0NoYXJhY3Rlcn0gcGxheWVyIFxuICAgKiBAcGFyYW0ge0NoYXJhY3Rlcn0gZW5lbXkgXG4gICAqIEBwYXJhbSB7TnVtYmVyfSBkYW1hZ2UgXG4gICAqL1xuICBjb21iYXRBY3Rpb24ocGxheWVyLCBlbmVteSwgZGFtYWdlID0gMCwgaGVhbCA9IGZhbHNlKSB7XG4gICAgbGV0IGNvbWJhdE92ZXIgPSBmYWxzZTtcbiAgICAvL2dlc3Rpb24gYmFycmUgZGUgdmllIHBsYXllclxuICAgIGxldCBwbGF5ZXJIZWFsdGhCYXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAjcGxheWVyMSAuYmxvYy1oZWFsdGggLmhlYWx0aGApO1xuICAgIC8vZ2VzdGlvbiBiYXJyZSBkZSB2aWUgZW5lbXlcbiAgICBsZXQgZW5lbXlIZWFsdGhCYXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAjZW5lbXkgLmJsb2MtaGVhbHRoIC5oZWFsdGhgKTtcbiAgICBpZiAoIWhlYWwpIHtcbiAgICAgIGVuZW15LmhlYWx0aCAtPSBkYW1hZ2U7XG4gICAgICBlbmVteS5tYW5hZ2VIZWFsdGgoKTtcbiAgICAgIHRoaXMuYWRkTG9nQ29tYmF0KHBsYXllci5uYW1lLCBlbmVteS5uYW1lLCBkYW1hZ2UpXG5cbiAgICAgIGlmIChlbmVteS5oZWFsdGggPiAwKSB7XG4gICAgICAgIGVuZW15SGVhbHRoQmFyLnRleHRDb250ZW50ID0gYCR7TWF0aC5mbG9vcigoZW5lbXkuaGVhbHRoIC8gZW5lbXkubWF4SGVhbHRoKSAqIDEwMCl9JWBcbiAgICAgICAgZW5lbXlIZWFsdGhCYXIuc3R5bGUud2lkdGggPSBgJHsoZW5lbXkuaGVhbHRoIC8gZW5lbXkubWF4SGVhbHRoKSAqIDEwMH0lYFxuICAgICAgfVxuICAgICAgZWxzZSB7XG4gICAgICAgIGVuZW15SGVhbHRoQmFyLnRleHRDb250ZW50ID0gYDAlYFxuICAgICAgICBlbmVteUhlYWx0aEJhci5zdHlsZS53aWR0aCA9IGAwJWBcbiAgICAgICAgdGhpcy5pc092ZXIoZW5lbXkpO1xuICAgICAgfVxuXG4gICAgICBjb25zb2xlLmxvZyhgZW5lbXkgaGVhbHRoICR7ZW5lbXkuaGVhbHRofSBhbmQgJHtkYW1hZ2V9IGRtZ2ApXG4gICAgICBpZiAoZW5lbXkuaGVhbHRoID4gMCkge1xuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICBsZXQgZW5lbXlEYW1hZ2UgPSBNYXRoLnJvdW5kKGVuZW15Lm1hZ2ljICogKE1hdGgucmFuZG9tKCkgKiAoMS41IC0gMSArIDEpICsgMSkpO1xuICAgICAgICAgIHBsYXllci5oZWFsdGggLT0gZW5lbXlEYW1hZ2UgO1xuXG4gICAgICAgICAgcGxheWVyLm1hbmFnZUhlYWx0aCgpO1xuXG4gICAgICAgICAgaWYgKHBsYXllci5oZWFsdGggPiAwKSB7XG4gICAgICAgICAgICBwbGF5ZXJIZWFsdGhCYXIudGV4dENvbnRlbnQgPSBgJHtNYXRoLmZsb29yKChwbGF5ZXIuaGVhbHRoIC8gcGxheWVyLm1heEhlYWx0aCkgKiAxMDApfSVgXG4gICAgICAgICAgICBwbGF5ZXJIZWFsdGhCYXIuc3R5bGUud2lkdGggPSBgJHsocGxheWVyLmhlYWx0aCAvIHBsYXllci5tYXhIZWFsdGgpICogMTAwfSVgXG4gICAgICAgICAgfVxuICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcGxheWVySGVhbHRoQmFyLnRleHRDb250ZW50ID0gYDAlYFxuICAgICAgICAgICAgcGxheWVySGVhbHRoQmFyLnN0eWxlLndpZHRoID0gYDAlYFxuICAgICAgICAgICAgdGhpcy5nYW1lT3ZlcigpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGNvbnNvbGUubG9nKGBQbGF5ZXIgaGVhbHRoICR7cGxheWVyLmhlYWx0aH0gYW5kICR7ZW5lbXkubWFnaWN9YClcblxuICAgICAgICAgIHRoaXMuYWRkTG9nQ29tYmF0KGVuZW15Lm5hbWUsIHBsYXllci5uYW1lLCBlbmVteURhbWFnZSlcblxuICAgICAgICB9LCAxMjAwKTtcbiAgICAgIH1cbiAgICB9XG4gICAgZWxzZSB7XG4gICAgICBwbGF5ZXIuaGVhbHRoICs9IDIyO1xuICAgICAgcGxheWVyLm1hbmFnZUhlYWx0aCgpO1xuICAgICAgLy90aGlzLmFkZExvZ0NvbWJhdCh0aGlzLmFkZExvZ0NvbWJhdChwbGF5ZXIubmFtZSwgcGxheWVyLm5hbWUsIC0yMikpXG5cbiAgICAgIGlmIChwbGF5ZXIuaGVhbHRoID4gMCkge1xuICAgICAgICBwbGF5ZXJIZWFsdGhCYXIudGV4dENvbnRlbnQgPSBgJHtNYXRoLmZsb29yKChwbGF5ZXIuaGVhbHRoIC8gcGxheWVyLm1heEhlYWx0aCkgKiAxMDApfSVgXG4gICAgICAgIHBsYXllckhlYWx0aEJhci5zdHlsZS53aWR0aCA9IGAkeyhwbGF5ZXIuaGVhbHRoIC8gcGxheWVyLm1heEhlYWx0aCkgKiAxMDB9JWBcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBwbGF5ZXJIZWFsdGhCYXIudGV4dENvbnRlbnQgPSBgMCVgXG4gICAgICAgIHBsYXllckhlYWx0aEJhci5zdHlsZS53aWR0aCA9IGAwJWBcbiAgICAgICAgdGhpcy5nYW1lT3ZlcigpO1xuICAgICAgfVxuXG4gICAgICBjb25zb2xlLmxvZyhwbGF5ZXIuaGVhbHRoKTtcblxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIGVuZW15LmhlYWx0aCArPSAyMjtcbiAgICAgICAgZW5lbXkubWFuYWdlSGVhbHRoKCk7XG4gICAgICAgIGNvbnNvbGUubG9nKGBlbmVteSBoZWFsdGggJHtlbmVteS5oZWFsdGh9YClcblxuICAgICAgICBpZiAoZW5lbXkuaGVhbHRoID4gMCkge1xuICAgICAgICAgIGVuZW15SGVhbHRoQmFyLnRleHRDb250ZW50ID0gYCR7TWF0aC5mbG9vcigoZW5lbXkuaGVhbHRoIC8gZW5lbXkubWF4SGVhbHRoKSAqIDEwMCl9JWBcbiAgICAgICAgICBlbmVteUhlYWx0aEJhci5zdHlsZS53aWR0aCA9IGAkeyhlbmVteS5oZWFsdGggLyBlbmVteS5tYXhIZWFsdGgpICogMTAwfSVgXG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgZW5lbXlIZWFsdGhCYXIudGV4dENvbnRlbnQgPSBgMCVgXG4gICAgICAgICAgZW5lbXlIZWFsdGhCYXIuc3R5bGUud2lkdGggPSBgMCVgXG4gICAgICAgICAgdGhpcy5pc092ZXIoZW5lbXkpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5hZGRMb2dDb21iYXQocGxheWVyLm5hbWUsIGVuZW15Lm5hbWUsIGRhbWFnZSlcblxuICAgICAgfSwgMTIwMCk7XG4gICAgfVxuXG4gIH1cblxuICBnYW1lT3ZlcigpIHtcbiAgICBpZiAoY29uZmlybSgnWW91IGFyZSBkZWFkLiBcXG4gQ2xpY2sgXFxcIk9rXFxcIiB0byByZXN0YXJ0IHdpdGggdGhpcyBjaGFyYWN0ZXIuXFxuIENsaWNrIFxcXCJBbm51bGVyXFxcIiB0byBjcmVhdCBhIG5ldyBjaGFyYWN0ZXInKSkge1xuICAgICAgbG9jYXRpb24ucmVsb2FkKCk7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgbG9jYXRpb24uaHJlZiA9IFwiY3JlYXRlLWNoYXJhY3Rlci5odG1sXCI7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gKiBcbiAqIEBwYXJhbSB7Q2hhcmFjdGVyfSBwbGF5ZXIgXG4gKiBAcGFyYW0ge2Jvb2xlYW59IGNhbkF0dGFjayBcbiAqIEBwYXJhbSB7Q2hhcmFjdGVyfSBlbmVteSBcbiAqIEBwYXJhbSB7bnVtYmVyfSBidXR0b25OdW1iZXIgXG4gKi9cbiAgY29tYmF0VmlldyhwbGF5ZXIsIGNhbkF0dGFjaywgZW5lbXksIGJ1dHRvbk51bWJlcikge1xuICAgIGlmIChjYW5BdHRhY2spIHtcbiAgICAgIGxldCBza2lsbCA9IG5ldyBTa2lsbChwbGF5ZXIsIGJ1dHRvbk51bWJlcik7XG4gICAgICBsZXQgYm9udXMgPSB0aGlzLmNvbXB1dGVCb251cyhwbGF5ZXIpXG4gICAgICBsZXQgZG1nID0gc2tpbGwuZGVhbEFsbERtZyhib251cyk7XG4gICAgICBcblxuICAgICAgY29uc29sZS5sb2coYE1heCBoZWFsdGggcGxheWVyIDogJHtwbGF5ZXIubWF4SGVhbHRofWApXG5cbiAgICAgIGNhbkF0dGFjayA9IGZhbHNlO1xuICAgICAgdGhpcy5jb21iYXRBY3Rpb24ocGxheWVyLCBlbmVteSwgZG1nKTtcbiAgICAgIGNvbnNvbGUubG9nKGRtZylcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICBjYW5BdHRhY2sgPSB0cnVlO1xuICAgICAgfSwgMTMwMCk7XG4gICAgICAvL2NvbnNvbGUubG9nKGAgcGR2IGFjdHVlbCA6ICR7cGxheWVyLmhlYWx0aH0gZGUgJHtwbGF5ZXIubmFtZX1gKVxuICAgICAgXG5cbiAgICAgIC8vY29uc29sZS5sb2coYCBwZHYgYWN0dWVsIHByZSBjb3VwOiAke2VuZW15LmhlYWx0aH0gZGUgJHtlbmVteS5uYW1lfWApXG4gICAgICBlbmVteS50YWtlRGFtYWdlKGRtZyk7XG4gICAgICBlbmVteS5tYW5hZ2VIZWFsdGgoKTtcblxuICAgICAgLy9jb25zb2xlLmxvZyhgIHBkdiBhY3R1ZWwgOiAke2VuZW15LmhlYWx0aH0gZGUgJHtlbmVteS5uYW1lfWApXG5cbiAgICAgIFxuICAgIH07XG4gIH07XG59Il0sInNvdXJjZVJvb3QiOiIifQ==