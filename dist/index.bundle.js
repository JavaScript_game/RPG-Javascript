/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/character.js":
/*!*****************************!*\
  !*** ./src/js/character.js ***!
  \*****************************/
/*! exports provided: Character */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Character", function() { return Character; });
/* harmony import */ var _json_skills_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../json/skills.json */ "./src/json/skills.json");
var _json_skills_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../json/skills.json */ "./src/json/skills.json", 1);
/* harmony import */ var _json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../json/jobAttributs.json */ "./src/json/jobAttributs.json");
var _json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../json/jobAttributs.json */ "./src/json/jobAttributs.json", 1);




class Character {
/**
 * This class defines a Character. It takes different stats as parameters.
 * Different methods are used to interact with other Characters or with itself.
 * Theses methods manage the level up part, the stats growth and the profession choice.
 * maxHealth and maxMana stores the maximum amount of theses value so that heals don't overcap this stat.
 * @param {string} name 
 * @param {string} profession 
 * @param {number} health 
 * @param {number} mana 
 * @param {number} strength 
 * @param {number} vitality 
 * @param {number} magic 
 */
  constructor (name, profession, health, mana, strength, vitality, magic, exp = 0, level = 0){
    this.name = name;
    this.profession = profession;
    this.health = health;
    this.maxHealth = health;
    this.mana = mana;
    this.maxMana = mana;
    this.strength = strength;
    this.vitality = vitality;
    this.magic = magic;
    this.exp = exp;
    this.level = level;

    this.alive = true;

    this.ability1=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[profession][0].name;
    this.ability2=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[profession][1].name;
    this.ability3=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[profession][2].name;
    this.ability4="Heal";
  }

  /**
   * 
   * Takes in a value and adds it to the stat 
   */
  upgradeStrength(value){
    this.strength = this.strength + value
  }
  upgradeVitality(value){
    this.vitality = this.vitality + value
  }
  upgradeMagic(value){
    this.magic = this.magic + value
  }
  upgradeHealth(value){
    this.health = this.health + value
    this.maxHealth = this.health;
  }
  upgradeMana(value){
    this.mana = this.mana + value
    this.maxMana = this.mana;
  }

  healHealth(value){
    this.health += value;
    this.manageHealth();
  }
  healMana(value){
    this.mana += value;
    this.manageMana();
  }
  // Prevents health and mana to have values superior to max value
  manageHealth(){
    if (this.health > this.maxHealth){
      this.health = this.maxHealth;
    }
  }
  manageMana(){
    if (this.mana > this.maxMana){
      this.mana = this.maxMana;
    }
  }

  /**
   * 
   * Experience and level up.
   * Experience goes from 0 to 100, when hit hits 100 or more, exp recieves exp -100 and the   character gains a level (levelup)
   */
  gainExp (experiencePoints){
    this.exp = this.exp + experiencePoints;
    if (this.exp === 100){
      this.level = this.level++
      this.levelUp();
    }
    else if (this.exp > 100){
      this.levelUp();
      this.exp = this.exp - 100;
    }
  }
  levelUp (){       // HERE PUT ALL PROFESSION AND STATS GROWTH maxD ON THEM
    this.level++;
    if (this.profession === "Wizard"){
      this.upgradeStrength(1);
      this.upgradeVitality(2);
      this.upgradeMagic(5);
      this.upgradeHealth(10);
      this.upgradeMana(20);
    }
    else if (this.profession === "Fighter"){
      this.upgradeStrength(4);
      this.upgradeVitality(2);
      this.upgradeMagic(2);
      this.upgradeHealth(20);
      this.upgradeMana(10);
    }
    else if (this.profession === "Thief"){
      this.upgradeStrength(5);
      this.upgradeVitality(1);
      this.upgradeMagic(2);
      this.upgradeHealth(15);
      this.upgradeMana(15);
    }
    else if (this.profession === "Paladin"){
      this.upgradeStrength(3);
      this.upgradeVitality(4);
      this.upgradeMagic(2);
      this.upgradeHealth(16);
      this.upgradeMana(14);
    }
  }

  /**
   * strength and magic stats are used to deal damages to enemies
   */
  dealPhysicalDmg(){
    return this.strength;
  }
  dealMagicDmg(){
    return this.magic;
  }

  /**
   *  Methods to take in damages and check is the character is dead
   */
  takeDamage(value){
    this.health -= value;
  }

  isHeDead (){
    if (this.health <= 0){
      this.alive = false;
    }
  }
}

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _skill__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./skill */ "./src/js/skill.js");
/* harmony import */ var _character__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./character */ "./src/js/character.js");
/* harmony import */ var _toolBox__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./toolBox */ "./src/js/toolBox.js");
/* harmony import */ var _jobSkills__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./jobSkills */ "./src/js/jobSkills.js");
/* harmony import */ var _json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../json/jobAttributs.json */ "./src/json/jobAttributs.json");
var _json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_4___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../json/jobAttributs.json */ "./src/json/jobAttributs.json", 1);
//import en mode webpack










//test de création d'un perso 
let tools = new _toolBox__WEBPACK_IMPORTED_MODULE_2__["Toolbox"]();
let player = JSON.parse(localStorage.getItem('mainCharacter'))
player = new _character__WEBPACK_IMPORTED_MODULE_1__["Character"](player.name, player.profession, player.health, player.mana, player.strength, player.vitality, player.magic);

let choiceEnemy = Math.round(Math.random() * (2 - 0) + 0);
let choiceEnemyJob = ["Mecabird", "Ghost", "Little Mage"];

let selectedEnemyJob = choiceEnemyJob[choiceEnemy];


//creating an enemy

let enemy = new _character__WEBPACK_IMPORTED_MODULE_1__["Character"](_json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_4__[selectedEnemyJob][0].name, selectedEnemyJob, _json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_4__[selectedEnemyJob][0].health, _json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_4__[selectedEnemyJob][0].mana, _json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_4__[selectedEnemyJob][0].strength, _json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_4__[selectedEnemyJob][0].vitality, _json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_4__[selectedEnemyJob][0].magic, 0, 0)
console.log(player);
console.log(enemy)





//definition of variable

let action1 = document.querySelector("#action1");
let action2 = document.querySelector("#action2");
let action3 = document.querySelector("#action3");
let action4 = document.querySelector("#action4");

let canAttack = true;


action1.addEventListener('click', function () {
  tools.combatView(player,canAttack,enemy,1);
});

action2.addEventListener('click', function () {
  tools.combatView(player,canAttack,enemy,2);
});

action3.addEventListener('click', function () {
  tools.combatView(player,canAttack,enemy,3);
});

action4.addEventListener('click', function () {

  if (canAttack) {
    let bonus = tools.computeBonus(player)
    let skill = new _skill__WEBPACK_IMPORTED_MODULE_0__["Skill"](player, 4);
    let dmg = -(skill.dealAllDmg(bonus));

    canAttack = false;
    tools.combatAction(player, enemy, dmg, true);
    setTimeout(() => {
      canAttack = true;
    }, 1300);

    //gestion barre de vie player
    let playerHealthBar = document.querySelector(`#player1 .bloc-health .health`);
    //console.log(` pdv actuel pre coup: ${player.health}`)
    player.manageHealth();


    console.log(` pdv actuel : ${player.health}`)

    if (player.health > 0) {
      playerHealthBar.textContent = `${Math.floor((player.health / player.maxHealth) * 100)}%`
      playerHealthBar.style.width = `${(player.health / player.maxHealth) * 100}%`
    }
    else {
      playerHealthBar.textContent = `0%`
      playerHealthBar.style.width = `0%`
      console.log('ded');
    }

    //gestion barre de vie enemy
    let enemyHealthBar = document.querySelector(`#enemy .bloc-health .health`);
    //console.log(` pdv actuel pre coup: ${enemy.health}`)
    enemy.takeDamage(dmg);
    enemy.manageHealth()


  }
});


//affichage des compétences
action1.textContent = player.ability1;
action2.textContent = player.ability2;
action3.textContent = player.ability3;
action4.textContent = player.ability4;

//gestion des noms de perso
let player1 = document.querySelector(`#player1>p`)
player1.textContent = player.name;

let opponent = document.querySelector(`#enemy>p`)
opponent.textContent = enemy.name;

//gestion du skin enemy

let enemyId = document.querySelector('.gameScreen>div')

if (enemy.profession === "Ghost") {
  enemyId.id = "Ghost";
}
else if (enemy.profession === "Mecabird") {
  enemyId.id = "Mecabird";
}
else if (enemy.profession === "Little Mage") {
  enemyId.id = "Little-Mage";
};

/***/ }),

/***/ "./src/js/jobSkills.js":
/*!*****************************!*\
  !*** ./src/js/jobSkills.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _skill__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./skill */ "./src/js/skill.js");
/* harmony import */ var _json_skills_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../json/skills.json */ "./src/json/skills.json");
var _json_skills_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../json/skills.json */ "./src/json/skills.json", 1);


// let variab = "thief";
// console.log(skillList[`${variab}`][0].name);
// console.log(skillList[perso.profession][0].name)




/***/ }),

/***/ "./src/js/skill.js":
/*!*************************!*\
  !*** ./src/js/skill.js ***!
  \*************************/
/*! exports provided: Skill */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Skill", function() { return Skill; });
/* harmony import */ var _json_skills_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../json/skills.json */ "./src/json/skills.json");
var _json_skills_json__WEBPACK_IMPORTED_MODULE_0___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../json/skills.json */ "./src/json/skills.json", 1);
 
class Skill {
  constructor(perso,action=1){
    this.waterDmg=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[perso.profession][action-1].water,
    this.fireDmg=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[perso.profession][action-1].fire,
    this.earthDmg=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[perso.profession][action-1].earth,
    this.shadowDmg=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[perso.profession][action-1].shadow,
    this.skillName=_json_skills_json__WEBPACK_IMPORTED_MODULE_0__[perso.profession][action-1].name
  }

  dealWaterDmg(bonus){
    let water=parseInt(this.waterDmg);
    return water*bonus
  }

  dealFireDmg(bonus){
    let fire=parseInt(this.fireDmg);
    return fire*bonus
  }

  dealEarthDmg(bonus){
   let  earth=parseInt(this.earthDmg);
    return earth*bonus
  }

  dealShadowDmg(bonus){
    let shadow=parseInt(this.shadowDmg);
    return shadow*bonus
  }

  dealAllDmg(bonus){
    return this.dealWaterDmg(bonus)+this.dealEarthDmg(bonus)+this.dealFireDmg(bonus)+this.dealShadowDmg(bonus)
  }
}

/***/ }),

/***/ "./src/js/toolBox.js":
/*!***************************!*\
  !*** ./src/js/toolBox.js ***!
  \***************************/
/*! exports provided: Toolbox */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Toolbox", function() { return Toolbox; });
/* harmony import */ var _character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./character */ "./src/js/character.js");
/* harmony import */ var _json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../json/jobAttributs.json */ "./src/json/jobAttributs.json");
var _json_jobAttributs_json__WEBPACK_IMPORTED_MODULE_1___namespace = /*#__PURE__*/__webpack_require__.t(/*! ../json/jobAttributs.json */ "./src/json/jobAttributs.json", 1);
/* harmony import */ var _skill__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./skill */ "./src/js/skill.js");




class Toolbox {
  constructor() {

  }
  createCharacter(name, profession) {

    if (profession === "Wizard") {
      let character = new _character__WEBPACK_IMPORTED_MODULE_0__["Character"](name, profession, 160, 200, 10, 10, 20);
      return character;
    }
    else if (profession === "Fighter") {
      let character = new _character__WEBPACK_IMPORTED_MODULE_0__["Character"](name, profession, 200, 100, 18, 12, 10);
      return character;
    }
    else if (profession === "Thief") {
      let character = new _character__WEBPACK_IMPORTED_MODULE_0__["Character"](name, profession, 180, 150, 20, 10, 10);
      return character;
    }
    else if (profession === "Paladin") {
      let character = new _character__WEBPACK_IMPORTED_MODULE_0__["Character"](name, profession, 190, 140, 13, 15, 12);
      return character;
    }
  }

  /**
 * 
 * @param {string} from the source of the damage
 * @param {string} to the char wounded
 * @param {number} dmg the number of damage
 */
  addLogCombat(from, to, dmg) {
    let logCombat = document.createElement('p');
    logCombat.textContent = `${to} takes ${dmg} damages from ${from}`;

    let logScroll = document.querySelector('#scroll');
    logScroll.prepend(logCombat);

    return dmg;
  }

  /**
 * 
 * @param {Character} character character object
 */
  computeBonus(character) {

    let randomizer = Math.random() * (1.5 - 1 + 1) + 1;


    if (character.profession === "Fighter") {

      let skill = Math.round(character.strength * randomizer);
      return skill / 10;
    }

    else if (character.profession === "Wizard") {

      let skill = Math.round(character.magic * randomizer);
      return skill / 10;
    }

    else if (character.profession === "Thief") {

      let skill = Math.round(character.strength + 5 * randomizer);
      return skill / 10;
    }

    else if (character.profession === "Paladin") {

      console.log(character.strength);

      let skill = Math.round((character.strength + character.magic) / 2 * randomizer)

      return (skill / 10);
    }

  }

  /**
 * Verifies if the fighters are alive. When one dies, his alive prop turn to false
 * @param {Character} player 
 * @param {Character} enemy 
 */
  isOver(enemy) {
    if (enemy.health <= 0) {
      enemy.alive = false;
      alert(`${enemy.name} is defeated. \n Congratulation!`);
      location.reload();
    }
  }

  /**
   * Launches a battle phase between two Characters. When the player does an action, the enemy replies after 1.2 seconds. Player can't play for 2 seconds
   * @param {Character} player 
   * @param {Character} enemy 
   * @param {Number} damage 
   */
  combatAction(player, enemy, damage = 0, heal = false) {
    let combatOver = false;
    //gestion barre de vie player
    let playerHealthBar = document.querySelector(`#player1 .bloc-health .health`);
    //gestion barre de vie enemy
    let enemyHealthBar = document.querySelector(`#enemy .bloc-health .health`);
    if (!heal) {
      enemy.health -= damage;
      enemy.manageHealth();
      this.addLogCombat(player.name, enemy.name, damage)

      if (enemy.health > 0) {
        enemyHealthBar.textContent = `${Math.floor((enemy.health / enemy.maxHealth) * 100)}%`
        enemyHealthBar.style.width = `${(enemy.health / enemy.maxHealth) * 100}%`
      }
      else {
        enemyHealthBar.textContent = `0%`
        enemyHealthBar.style.width = `0%`
        this.isOver(enemy);
      }

      console.log(`enemy health ${enemy.health} and ${damage} dmg`)
      if (enemy.health > 0) {
        setTimeout(() => {
          let enemyDamage = Math.round(enemy.magic * (Math.random() * (1.5 - 1 + 1) + 1));
          player.health -= enemyDamage ;

          player.manageHealth();

          if (player.health > 0) {
            playerHealthBar.textContent = `${Math.floor((player.health / player.maxHealth) * 100)}%`
            playerHealthBar.style.width = `${(player.health / player.maxHealth) * 100}%`
          }
          else {
            playerHealthBar.textContent = `0%`
            playerHealthBar.style.width = `0%`
            this.gameOver();
          }

          console.log(`Player health ${player.health} and ${enemy.magic}`)

          this.addLogCombat(enemy.name, player.name, enemyDamage)

        }, 1200);
      }
    }
    else {
      player.health += 22;
      player.manageHealth();
      //this.addLogCombat(this.addLogCombat(player.name, player.name, -22))

      if (player.health > 0) {
        playerHealthBar.textContent = `${Math.floor((player.health / player.maxHealth) * 100)}%`
        playerHealthBar.style.width = `${(player.health / player.maxHealth) * 100}%`
      }
      else {
        playerHealthBar.textContent = `0%`
        playerHealthBar.style.width = `0%`
        this.gameOver();
      }

      console.log(player.health);

      setTimeout(() => {
        enemy.health += 22;
        enemy.manageHealth();
        console.log(`enemy health ${enemy.health}`)

        if (enemy.health > 0) {
          enemyHealthBar.textContent = `${Math.floor((enemy.health / enemy.maxHealth) * 100)}%`
          enemyHealthBar.style.width = `${(enemy.health / enemy.maxHealth) * 100}%`
        }
        else {
          enemyHealthBar.textContent = `0%`
          enemyHealthBar.style.width = `0%`
          this.isOver(enemy);
        }

        this.addLogCombat(player.name, enemy.name, damage)

      }, 1200);
    }

  }

  gameOver() {
    if (confirm('You are dead. \n Click \"Ok\" to restart with this character.\n Click \"Annuler\" to creat a new character')) {
      location.reload();
    }
    else {
      location.href = "create-character.html";
    }
  }

  /**
 * 
 * @param {Character} player 
 * @param {boolean} canAttack 
 * @param {Character} enemy 
 * @param {number} buttonNumber 
 */
  combatView(player, canAttack, enemy, buttonNumber) {
    if (canAttack) {
      let skill = new _skill__WEBPACK_IMPORTED_MODULE_2__["Skill"](player, buttonNumber);
      let bonus = this.computeBonus(player)
      let dmg = skill.dealAllDmg(bonus);
      

      console.log(`Max health player : ${player.maxHealth}`)

      canAttack = false;
      this.combatAction(player, enemy, dmg);
      console.log(dmg)
      setTimeout(() => {
        canAttack = true;
      }, 1300);
      //console.log(` pdv actuel : ${player.health} de ${player.name}`)
      

      //console.log(` pdv actuel pre coup: ${enemy.health} de ${enemy.name}`)
      enemy.takeDamage(dmg);
      enemy.manageHealth();

      //console.log(` pdv actuel : ${enemy.health} de ${enemy.name}`)

      
    };
  };
}

/***/ }),

/***/ "./src/json/jobAttributs.json":
/*!************************************!*\
  !*** ./src/json/jobAttributs.json ***!
  \************************************/
/*! exports provided: Fighter, Wizard, Thief, Paladin, Ghost, Mecabird, Little Mage, default */
/***/ (function(module) {

module.exports = {"Fighter":[{"health":200,"mana":100,"strength":18,"vitality":12,"magic":10}],"Wizard":[{"health":100,"mana":200,"strength":10,"vitality":10,"magic":20}],"Thief":[{"health":150,"mana":150,"strength":20,"vitality":10,"magic":10}],"Paladin":[{"health":160,"mana":140,"strength":13,"vitality":15,"magic":12}],"Ghost":[{"name":"Ugly Casper","health":290,"mana":140,"strength":12,"vitality":15,"magic":23}],"Mecabird":[{"name":"Garurumon","health":250,"mana":140,"strength":12,"vitality":15,"magic":22}],"Little Mage":[{"name":"Jessica","health":310,"mana":140,"strength":13,"vitality":15,"magic":23}]};

/***/ }),

/***/ "./src/json/skills.json":
/*!******************************!*\
  !*** ./src/json/skills.json ***!
  \******************************/
/*! exports provided: Fighter, Thief, Wizard, Paladin, Little Mage, Mecabird, Ghost, default */
/***/ (function(module) {

module.exports = {"Fighter":[{"water":0,"fire":0,"earth":10,"shadow":0,"name":"Gaïa Clash"},{"water":0,"fire":10,"earth":0,"shadow":0,"name":"Fire Slash"},{"water":10,"fire":0,"earth":0,"shadow":0,"name":"Neptune hit"},{"water":10,"fire":10,"earth":0,"shadow":2,"name":"Heal sword"}],"Thief":[{"water":4,"fire":4,"earth":2,"shadow":0,"name":"Mug !"},{"water":2,"fire":4,"earth":4,"shadow":0,"name":"Back stab !"},{"water":0,"fire":0,"earth":0,"shadow":10,"name":"Slice in the night"},{"water":10,"fire":10,"earth":0,"shadow":2,"name":"Heal "}],"Wizard":[{"water":5,"fire":3,"earth":2,"shadow":0,"name":"Vanishing Bloom"},{"water":0,"fire":5,"earth":2,"shadow":2,"name":"Breath of dragoon"},{"water":5,"fire":0,"earth":0,"shadow":5,"name":"Valkyre of Poseidon"},{"water":10,"fire":10,"earth":0,"shadow":2,"name":"Neptune hit"}],"Paladin":[{"water":0,"fire":8,"earth":2,"shadow":0,"name":"Hammer of blossom"},{"water":0,"fire":4,"earth":4,"shadow":0,"name":"Hit by the light"},{"water":0,"fire":0,"earth":0,"shadow":10,"name":"Thunder Storm"},{"water":10,"fire":10,"earth":0,"shadow":2,"name":"Neptune hit"}],"Little Mage":[{"water":0,"fire":8,"earth":2,"shadow":0,"name":"Hammer of blossom"},{"water":0,"fire":4,"earth":4,"shadow":0,"name":"Hit by the light"},{"water":0,"fire":0,"earth":0,"shadow":10,"name":"Thunder Storm"},{"water":10,"fire":10,"earth":0,"shadow":2,"name":"Neptune hit"}],"Mecabird":[{"water":0,"fire":8,"earth":2,"shadow":0,"name":"Hammer of blossom"},{"water":0,"fire":4,"earth":4,"shadow":0,"name":"Hit by the light"},{"water":0,"fire":0,"earth":0,"shadow":10,"name":"Thunder Storm"}],"Ghost":[{"water":0,"fire":8,"earth":2,"shadow":0,"name":"Hammer of blossom"},{"water":0,"fire":4,"earth":4,"shadow":0,"name":"Hit by the light"},{"water":0,"fire":0,"earth":0,"shadow":10,"name":"Thunder Storm"}]};

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2NoYXJhY3Rlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2pvYlNraWxscy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvc2tpbGwuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL3Rvb2xCb3guanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esa0RBQTBDLGdDQUFnQztBQUMxRTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGdFQUF3RCxrQkFBa0I7QUFDMUU7QUFDQSx5REFBaUQsY0FBYztBQUMvRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQXlDLGlDQUFpQztBQUMxRSx3SEFBZ0gsbUJBQW1CLEVBQUU7QUFDckk7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7O0FBR0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqRkE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQixXQUFXLE9BQU87QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdkpBO0FBQUE7QUFBQTs7QUFFZ0I7QUFDSTtBQUNGO0FBQ2xCO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7O0FBR0E7O0FBRUE7QUFDQTtBQUNBOzs7Ozs7QUFNQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7O0FBR0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQSwyQ0FBMkMsY0FBYztBQUN6RDs7O0FBR0EsaUNBQWlDLGNBQWM7O0FBRS9DO0FBQ0EsdUNBQXVDLHFEQUFxRDtBQUM1Rix1Q0FBdUMseUNBQXlDO0FBQ2hGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsMkNBQTJDLGFBQWE7QUFDeEQ7QUFDQTs7O0FBR0E7QUFDQSxDQUFDOzs7QUFHRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFOzs7Ozs7Ozs7Ozs7Ozs7O0FDekhnQjtBQUNoQjtBQUNBO0FBQ0EsNEJBQTRCLE9BQU87QUFDbkM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDakNvQjtBQUNwQjtBQUNjOztBQUVkO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxXQUFXLE9BQU87QUFDbEIsV0FBVyxPQUFPO0FBQ2xCLFdBQVcsT0FBTztBQUNsQjtBQUNBO0FBQ0E7QUFDQSwrQkFBK0IsR0FBRyxTQUFTLElBQUksZ0JBQWdCLEtBQUs7O0FBRXBFO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsV0FBVyxVQUFVO0FBQ3JCO0FBQ0E7O0FBRUE7OztBQUdBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLFdBQVcsVUFBVTtBQUNyQixXQUFXLFVBQVU7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLFdBQVc7QUFDMUI7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxhQUFhLFVBQVU7QUFDdkIsYUFBYSxVQUFVO0FBQ3ZCLGFBQWEsT0FBTztBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0Esd0NBQXdDLG1EQUFtRDtBQUMzRix3Q0FBd0MsdUNBQXVDO0FBQy9FO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxrQ0FBa0MsYUFBYSxPQUFPLE9BQU87QUFDN0Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSw2Q0FBNkMscURBQXFEO0FBQ2xHLDZDQUE2Qyx5Q0FBeUM7QUFDdEY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLHVDQUF1QyxjQUFjLE9BQU8sWUFBWTs7QUFFeEU7O0FBRUEsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHlDQUF5QyxxREFBcUQ7QUFDOUYseUNBQXlDLHlDQUF5QztBQUNsRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esb0NBQW9DLGFBQWE7O0FBRWpEO0FBQ0EsMENBQTBDLG1EQUFtRDtBQUM3RiwwQ0FBMEMsdUNBQXVDO0FBQ2pGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQSxPQUFPO0FBQ1A7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsV0FBVyxVQUFVO0FBQ3JCLFdBQVcsUUFBUTtBQUNuQixXQUFXLFVBQVU7QUFDckIsV0FBVyxPQUFPO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EseUNBQXlDLGlCQUFpQjs7QUFFMUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxxQ0FBcUMsY0FBYyxNQUFNLFlBQVk7OztBQUdyRSw2Q0FBNkMsYUFBYSxNQUFNLFdBQVc7QUFDM0U7QUFDQTs7QUFFQSxxQ0FBcUMsYUFBYSxNQUFNLFdBQVc7OztBQUduRTtBQUNBO0FBQ0EsQyIsImZpbGUiOiIuL2luZGV4LmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2pzL2luZGV4LmpzXCIpO1xuIiwiXG5pbXBvcnQgc2tpbGxMaXN0IGZyb20gJy4uL2pzb24vc2tpbGxzLmpzb24nXG5pbXBvcnQgbW9uc3RlcnMgZnJvbSBcIi4uL2pzb24vam9iQXR0cmlidXRzLmpzb25cIlxuXG5leHBvcnQgY2xhc3MgQ2hhcmFjdGVyIHtcbi8qKlxuICogVGhpcyBjbGFzcyBkZWZpbmVzIGEgQ2hhcmFjdGVyLiBJdCB0YWtlcyBkaWZmZXJlbnQgc3RhdHMgYXMgcGFyYW1ldGVycy5cbiAqIERpZmZlcmVudCBtZXRob2RzIGFyZSB1c2VkIHRvIGludGVyYWN0IHdpdGggb3RoZXIgQ2hhcmFjdGVycyBvciB3aXRoIGl0c2VsZi5cbiAqIFRoZXNlcyBtZXRob2RzIG1hbmFnZSB0aGUgbGV2ZWwgdXAgcGFydCwgdGhlIHN0YXRzIGdyb3d0aCBhbmQgdGhlIHByb2Zlc3Npb24gY2hvaWNlLlxuICogbWF4SGVhbHRoIGFuZCBtYXhNYW5hIHN0b3JlcyB0aGUgbWF4aW11bSBhbW91bnQgb2YgdGhlc2VzIHZhbHVlIHNvIHRoYXQgaGVhbHMgZG9uJ3Qgb3ZlcmNhcCB0aGlzIHN0YXQuXG4gKiBAcGFyYW0ge3N0cmluZ30gbmFtZSBcbiAqIEBwYXJhbSB7c3RyaW5nfSBwcm9mZXNzaW9uIFxuICogQHBhcmFtIHtudW1iZXJ9IGhlYWx0aCBcbiAqIEBwYXJhbSB7bnVtYmVyfSBtYW5hIFxuICogQHBhcmFtIHtudW1iZXJ9IHN0cmVuZ3RoIFxuICogQHBhcmFtIHtudW1iZXJ9IHZpdGFsaXR5IFxuICogQHBhcmFtIHtudW1iZXJ9IG1hZ2ljIFxuICovXG4gIGNvbnN0cnVjdG9yIChuYW1lLCBwcm9mZXNzaW9uLCBoZWFsdGgsIG1hbmEsIHN0cmVuZ3RoLCB2aXRhbGl0eSwgbWFnaWMsIGV4cCA9IDAsIGxldmVsID0gMCl7XG4gICAgdGhpcy5uYW1lID0gbmFtZTtcbiAgICB0aGlzLnByb2Zlc3Npb24gPSBwcm9mZXNzaW9uO1xuICAgIHRoaXMuaGVhbHRoID0gaGVhbHRoO1xuICAgIHRoaXMubWF4SGVhbHRoID0gaGVhbHRoO1xuICAgIHRoaXMubWFuYSA9IG1hbmE7XG4gICAgdGhpcy5tYXhNYW5hID0gbWFuYTtcbiAgICB0aGlzLnN0cmVuZ3RoID0gc3RyZW5ndGg7XG4gICAgdGhpcy52aXRhbGl0eSA9IHZpdGFsaXR5O1xuICAgIHRoaXMubWFnaWMgPSBtYWdpYztcbiAgICB0aGlzLmV4cCA9IGV4cDtcbiAgICB0aGlzLmxldmVsID0gbGV2ZWw7XG5cbiAgICB0aGlzLmFsaXZlID0gdHJ1ZTtcblxuICAgIHRoaXMuYWJpbGl0eTE9c2tpbGxMaXN0W3Byb2Zlc3Npb25dWzBdLm5hbWU7XG4gICAgdGhpcy5hYmlsaXR5Mj1za2lsbExpc3RbcHJvZmVzc2lvbl1bMV0ubmFtZTtcbiAgICB0aGlzLmFiaWxpdHkzPXNraWxsTGlzdFtwcm9mZXNzaW9uXVsyXS5uYW1lO1xuICAgIHRoaXMuYWJpbGl0eTQ9XCJIZWFsXCI7XG4gIH1cblxuICAvKipcbiAgICogXG4gICAqIFRha2VzIGluIGEgdmFsdWUgYW5kIGFkZHMgaXQgdG8gdGhlIHN0YXQgXG4gICAqL1xuICB1cGdyYWRlU3RyZW5ndGgodmFsdWUpe1xuICAgIHRoaXMuc3RyZW5ndGggPSB0aGlzLnN0cmVuZ3RoICsgdmFsdWVcbiAgfVxuICB1cGdyYWRlVml0YWxpdHkodmFsdWUpe1xuICAgIHRoaXMudml0YWxpdHkgPSB0aGlzLnZpdGFsaXR5ICsgdmFsdWVcbiAgfVxuICB1cGdyYWRlTWFnaWModmFsdWUpe1xuICAgIHRoaXMubWFnaWMgPSB0aGlzLm1hZ2ljICsgdmFsdWVcbiAgfVxuICB1cGdyYWRlSGVhbHRoKHZhbHVlKXtcbiAgICB0aGlzLmhlYWx0aCA9IHRoaXMuaGVhbHRoICsgdmFsdWVcbiAgICB0aGlzLm1heEhlYWx0aCA9IHRoaXMuaGVhbHRoO1xuICB9XG4gIHVwZ3JhZGVNYW5hKHZhbHVlKXtcbiAgICB0aGlzLm1hbmEgPSB0aGlzLm1hbmEgKyB2YWx1ZVxuICAgIHRoaXMubWF4TWFuYSA9IHRoaXMubWFuYTtcbiAgfVxuXG4gIGhlYWxIZWFsdGgodmFsdWUpe1xuICAgIHRoaXMuaGVhbHRoICs9IHZhbHVlO1xuICAgIHRoaXMubWFuYWdlSGVhbHRoKCk7XG4gIH1cbiAgaGVhbE1hbmEodmFsdWUpe1xuICAgIHRoaXMubWFuYSArPSB2YWx1ZTtcbiAgICB0aGlzLm1hbmFnZU1hbmEoKTtcbiAgfVxuICAvLyBQcmV2ZW50cyBoZWFsdGggYW5kIG1hbmEgdG8gaGF2ZSB2YWx1ZXMgc3VwZXJpb3IgdG8gbWF4IHZhbHVlXG4gIG1hbmFnZUhlYWx0aCgpe1xuICAgIGlmICh0aGlzLmhlYWx0aCA+IHRoaXMubWF4SGVhbHRoKXtcbiAgICAgIHRoaXMuaGVhbHRoID0gdGhpcy5tYXhIZWFsdGg7XG4gICAgfVxuICB9XG4gIG1hbmFnZU1hbmEoKXtcbiAgICBpZiAodGhpcy5tYW5hID4gdGhpcy5tYXhNYW5hKXtcbiAgICAgIHRoaXMubWFuYSA9IHRoaXMubWF4TWFuYTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogXG4gICAqIEV4cGVyaWVuY2UgYW5kIGxldmVsIHVwLlxuICAgKiBFeHBlcmllbmNlIGdvZXMgZnJvbSAwIHRvIDEwMCwgd2hlbiBoaXQgaGl0cyAxMDAgb3IgbW9yZSwgZXhwIHJlY2lldmVzIGV4cCAtMTAwIGFuZCB0aGUgICBjaGFyYWN0ZXIgZ2FpbnMgYSBsZXZlbCAobGV2ZWx1cClcbiAgICovXG4gIGdhaW5FeHAgKGV4cGVyaWVuY2VQb2ludHMpe1xuICAgIHRoaXMuZXhwID0gdGhpcy5leHAgKyBleHBlcmllbmNlUG9pbnRzO1xuICAgIGlmICh0aGlzLmV4cCA9PT0gMTAwKXtcbiAgICAgIHRoaXMubGV2ZWwgPSB0aGlzLmxldmVsKytcbiAgICAgIHRoaXMubGV2ZWxVcCgpO1xuICAgIH1cbiAgICBlbHNlIGlmICh0aGlzLmV4cCA+IDEwMCl7XG4gICAgICB0aGlzLmxldmVsVXAoKTtcbiAgICAgIHRoaXMuZXhwID0gdGhpcy5leHAgLSAxMDA7XG4gICAgfVxuICB9XG4gIGxldmVsVXAgKCl7ICAgICAgIC8vIEhFUkUgUFVUIEFMTCBQUk9GRVNTSU9OIEFORCBTVEFUUyBHUk9XVEggbWF4RCBPTiBUSEVNXG4gICAgdGhpcy5sZXZlbCsrO1xuICAgIGlmICh0aGlzLnByb2Zlc3Npb24gPT09IFwiV2l6YXJkXCIpe1xuICAgICAgdGhpcy51cGdyYWRlU3RyZW5ndGgoMSk7XG4gICAgICB0aGlzLnVwZ3JhZGVWaXRhbGl0eSgyKTtcbiAgICAgIHRoaXMudXBncmFkZU1hZ2ljKDUpO1xuICAgICAgdGhpcy51cGdyYWRlSGVhbHRoKDEwKTtcbiAgICAgIHRoaXMudXBncmFkZU1hbmEoMjApO1xuICAgIH1cbiAgICBlbHNlIGlmICh0aGlzLnByb2Zlc3Npb24gPT09IFwiRmlnaHRlclwiKXtcbiAgICAgIHRoaXMudXBncmFkZVN0cmVuZ3RoKDQpO1xuICAgICAgdGhpcy51cGdyYWRlVml0YWxpdHkoMik7XG4gICAgICB0aGlzLnVwZ3JhZGVNYWdpYygyKTtcbiAgICAgIHRoaXMudXBncmFkZUhlYWx0aCgyMCk7XG4gICAgICB0aGlzLnVwZ3JhZGVNYW5hKDEwKTtcbiAgICB9XG4gICAgZWxzZSBpZiAodGhpcy5wcm9mZXNzaW9uID09PSBcIlRoaWVmXCIpe1xuICAgICAgdGhpcy51cGdyYWRlU3RyZW5ndGgoNSk7XG4gICAgICB0aGlzLnVwZ3JhZGVWaXRhbGl0eSgxKTtcbiAgICAgIHRoaXMudXBncmFkZU1hZ2ljKDIpO1xuICAgICAgdGhpcy51cGdyYWRlSGVhbHRoKDE1KTtcbiAgICAgIHRoaXMudXBncmFkZU1hbmEoMTUpO1xuICAgIH1cbiAgICBlbHNlIGlmICh0aGlzLnByb2Zlc3Npb24gPT09IFwiUGFsYWRpblwiKXtcbiAgICAgIHRoaXMudXBncmFkZVN0cmVuZ3RoKDMpO1xuICAgICAgdGhpcy51cGdyYWRlVml0YWxpdHkoNCk7XG4gICAgICB0aGlzLnVwZ3JhZGVNYWdpYygyKTtcbiAgICAgIHRoaXMudXBncmFkZUhlYWx0aCgxNik7XG4gICAgICB0aGlzLnVwZ3JhZGVNYW5hKDE0KTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogc3RyZW5ndGggYW5kIG1hZ2ljIHN0YXRzIGFyZSB1c2VkIHRvIGRlYWwgZGFtYWdlcyB0byBlbmVtaWVzXG4gICAqL1xuICBkZWFsUGh5c2ljYWxEbWcoKXtcbiAgICByZXR1cm4gdGhpcy5zdHJlbmd0aDtcbiAgfVxuICBkZWFsTWFnaWNEbWcoKXtcbiAgICByZXR1cm4gdGhpcy5tYWdpYztcbiAgfVxuXG4gIC8qKlxuICAgKiAgTWV0aG9kcyB0byB0YWtlIGluIGRhbWFnZXMgYW5kIGNoZWNrIGlzIHRoZSBjaGFyYWN0ZXIgaXMgZGVhZFxuICAgKi9cbiAgdGFrZURhbWFnZSh2YWx1ZSl7XG4gICAgdGhpcy5oZWFsdGggLT0gdmFsdWU7XG4gIH1cblxuICBpc0hlRGVhZCAoKXtcbiAgICBpZiAodGhpcy5oZWFsdGggPD0gMCl7XG4gICAgICB0aGlzLmFsaXZlID0gZmFsc2U7XG4gICAgfVxuICB9XG59IiwiLy9pbXBvcnQgZW4gbW9kZSB3ZWJwYWNrXG5cbmltcG9ydCB7IFNraWxsIH0gZnJvbSBcIi4vc2tpbGxcIjtcbmltcG9ydCB7IENoYXJhY3RlciB9IGZyb20gXCIuL2NoYXJhY3RlclwiO1xuaW1wb3J0IHsgVG9vbGJveCB9IGZyb20gXCIuL3Rvb2xCb3hcIjtcbmltcG9ydCAnLi9qb2JTa2lsbHMnO1xuaW1wb3J0IG1vbnN0ZXJzIGZyb20gXCIuLi9qc29uL2pvYkF0dHJpYnV0cy5qc29uXCI7XG5cblxuXG5cbi8vdGVzdCBkZSBjcsOpYXRpb24gZCd1biBwZXJzbyBcbmxldCB0b29scyA9IG5ldyBUb29sYm94KCk7XG5sZXQgcGxheWVyID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbWFpbkNoYXJhY3RlcicpKVxucGxheWVyID0gbmV3IENoYXJhY3RlcihwbGF5ZXIubmFtZSwgcGxheWVyLnByb2Zlc3Npb24sIHBsYXllci5oZWFsdGgsIHBsYXllci5tYW5hLCBwbGF5ZXIuc3RyZW5ndGgsIHBsYXllci52aXRhbGl0eSwgcGxheWVyLm1hZ2ljKTtcblxubGV0IGNob2ljZUVuZW15ID0gTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpICogKDIgLSAwKSArIDApO1xubGV0IGNob2ljZUVuZW15Sm9iID0gW1wiTWVjYWJpcmRcIiwgXCJHaG9zdFwiLCBcIkxpdHRsZSBNYWdlXCJdO1xuXG5sZXQgc2VsZWN0ZWRFbmVteUpvYiA9IGNob2ljZUVuZW15Sm9iW2Nob2ljZUVuZW15XTtcblxuXG4vL2NyZWF0aW5nIGFuIGVuZW15XG5cbmxldCBlbmVteSA9IG5ldyBDaGFyYWN0ZXIobW9uc3RlcnNbc2VsZWN0ZWRFbmVteUpvYl1bMF0ubmFtZSwgc2VsZWN0ZWRFbmVteUpvYiwgbW9uc3RlcnNbc2VsZWN0ZWRFbmVteUpvYl1bMF0uaGVhbHRoLCBtb25zdGVyc1tzZWxlY3RlZEVuZW15Sm9iXVswXS5tYW5hLCBtb25zdGVyc1tzZWxlY3RlZEVuZW15Sm9iXVswXS5zdHJlbmd0aCwgbW9uc3RlcnNbc2VsZWN0ZWRFbmVteUpvYl1bMF0udml0YWxpdHksIG1vbnN0ZXJzW3NlbGVjdGVkRW5lbXlKb2JdWzBdLm1hZ2ljLCAwLCAwKVxuY29uc29sZS5sb2cocGxheWVyKTtcbmNvbnNvbGUubG9nKGVuZW15KVxuXG5cblxuXG5cbi8vZGVmaW5pdGlvbiBvZiB2YXJpYWJsZVxuXG5sZXQgYWN0aW9uMSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjYWN0aW9uMVwiKTtcbmxldCBhY3Rpb24yID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNhY3Rpb24yXCIpO1xubGV0IGFjdGlvbjMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2FjdGlvbjNcIik7XG5sZXQgYWN0aW9uNCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjYWN0aW9uNFwiKTtcblxubGV0IGNhbkF0dGFjayA9IHRydWU7XG5cblxuYWN0aW9uMS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgdG9vbHMuY29tYmF0VmlldyhwbGF5ZXIsY2FuQXR0YWNrLGVuZW15LDEpO1xufSk7XG5cbmFjdGlvbjIuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gIHRvb2xzLmNvbWJhdFZpZXcocGxheWVyLGNhbkF0dGFjayxlbmVteSwyKTtcbn0pO1xuXG5hY3Rpb24zLmFkZEV2ZW50TGlzdGVuZXIoJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICB0b29scy5jb21iYXRWaWV3KHBsYXllcixjYW5BdHRhY2ssZW5lbXksMyk7XG59KTtcblxuYWN0aW9uNC5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcblxuICBpZiAoY2FuQXR0YWNrKSB7XG4gICAgbGV0IGJvbnVzID0gdG9vbHMuY29tcHV0ZUJvbnVzKHBsYXllcilcbiAgICBsZXQgc2tpbGwgPSBuZXcgU2tpbGwocGxheWVyLCA0KTtcbiAgICBsZXQgZG1nID0gLShza2lsbC5kZWFsQWxsRG1nKGJvbnVzKSk7XG5cbiAgICBjYW5BdHRhY2sgPSBmYWxzZTtcbiAgICB0b29scy5jb21iYXRBY3Rpb24ocGxheWVyLCBlbmVteSwgZG1nLCB0cnVlKTtcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIGNhbkF0dGFjayA9IHRydWU7XG4gICAgfSwgMTMwMCk7XG5cbiAgICAvL2dlc3Rpb24gYmFycmUgZGUgdmllIHBsYXllclxuICAgIGxldCBwbGF5ZXJIZWFsdGhCYXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAjcGxheWVyMSAuYmxvYy1oZWFsdGggLmhlYWx0aGApO1xuICAgIC8vY29uc29sZS5sb2coYCBwZHYgYWN0dWVsIHByZSBjb3VwOiAke3BsYXllci5oZWFsdGh9YClcbiAgICBwbGF5ZXIubWFuYWdlSGVhbHRoKCk7XG5cblxuICAgIGNvbnNvbGUubG9nKGAgcGR2IGFjdHVlbCA6ICR7cGxheWVyLmhlYWx0aH1gKVxuXG4gICAgaWYgKHBsYXllci5oZWFsdGggPiAwKSB7XG4gICAgICBwbGF5ZXJIZWFsdGhCYXIudGV4dENvbnRlbnQgPSBgJHtNYXRoLmZsb29yKChwbGF5ZXIuaGVhbHRoIC8gcGxheWVyLm1heEhlYWx0aCkgKiAxMDApfSVgXG4gICAgICBwbGF5ZXJIZWFsdGhCYXIuc3R5bGUud2lkdGggPSBgJHsocGxheWVyLmhlYWx0aCAvIHBsYXllci5tYXhIZWFsdGgpICogMTAwfSVgXG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgcGxheWVySGVhbHRoQmFyLnRleHRDb250ZW50ID0gYDAlYFxuICAgICAgcGxheWVySGVhbHRoQmFyLnN0eWxlLndpZHRoID0gYDAlYFxuICAgICAgY29uc29sZS5sb2coJ2RlZCcpO1xuICAgIH1cblxuICAgIC8vZ2VzdGlvbiBiYXJyZSBkZSB2aWUgZW5lbXlcbiAgICBsZXQgZW5lbXlIZWFsdGhCYXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAjZW5lbXkgLmJsb2MtaGVhbHRoIC5oZWFsdGhgKTtcbiAgICAvL2NvbnNvbGUubG9nKGAgcGR2IGFjdHVlbCBwcmUgY291cDogJHtlbmVteS5oZWFsdGh9YClcbiAgICBlbmVteS50YWtlRGFtYWdlKGRtZyk7XG4gICAgZW5lbXkubWFuYWdlSGVhbHRoKClcblxuXG4gIH1cbn0pO1xuXG5cbi8vYWZmaWNoYWdlIGRlcyBjb21ww6l0ZW5jZXNcbmFjdGlvbjEudGV4dENvbnRlbnQgPSBwbGF5ZXIuYWJpbGl0eTE7XG5hY3Rpb24yLnRleHRDb250ZW50ID0gcGxheWVyLmFiaWxpdHkyO1xuYWN0aW9uMy50ZXh0Q29udGVudCA9IHBsYXllci5hYmlsaXR5MztcbmFjdGlvbjQudGV4dENvbnRlbnQgPSBwbGF5ZXIuYWJpbGl0eTQ7XG5cbi8vZ2VzdGlvbiBkZXMgbm9tcyBkZSBwZXJzb1xubGV0IHBsYXllcjEgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGAjcGxheWVyMT5wYClcbnBsYXllcjEudGV4dENvbnRlbnQgPSBwbGF5ZXIubmFtZTtcblxubGV0IG9wcG9uZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgI2VuZW15PnBgKVxub3Bwb25lbnQudGV4dENvbnRlbnQgPSBlbmVteS5uYW1lO1xuXG4vL2dlc3Rpb24gZHUgc2tpbiBlbmVteVxuXG5sZXQgZW5lbXlJZCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5nYW1lU2NyZWVuPmRpdicpXG5cbmlmIChlbmVteS5wcm9mZXNzaW9uID09PSBcIkdob3N0XCIpIHtcbiAgZW5lbXlJZC5pZCA9IFwiR2hvc3RcIjtcbn1cbmVsc2UgaWYgKGVuZW15LnByb2Zlc3Npb24gPT09IFwiTWVjYWJpcmRcIikge1xuICBlbmVteUlkLmlkID0gXCJNZWNhYmlyZFwiO1xufVxuZWxzZSBpZiAoZW5lbXkucHJvZmVzc2lvbiA9PT0gXCJMaXR0bGUgTWFnZVwiKSB7XG4gIGVuZW15SWQuaWQgPSBcIkxpdHRsZS1NYWdlXCI7XG59OyIsImltcG9ydCB7IFNraWxsIH0gZnJvbSBcIi4vc2tpbGxcIjtcbmltcG9ydCBza2lsbExpc3QgZnJvbSAnLi4vanNvbi9za2lsbHMuanNvbic7XG4vLyBsZXQgdmFyaWFiID0gXCJ0aGllZlwiO1xuLy8gY29uc29sZS5sb2coc2tpbGxMaXN0W2Ake3ZhcmlhYn1gXVswXS5uYW1lKTtcbi8vIGNvbnNvbGUubG9nKHNraWxsTGlzdFtwZXJzby5wcm9mZXNzaW9uXVswXS5uYW1lKVxuXG5cbiIsImltcG9ydCBza2lsbHNldCBmcm9tICcuLi9qc29uL3NraWxscy5qc29uJyBcbmV4cG9ydCBjbGFzcyBTa2lsbCB7XG4gIGNvbnN0cnVjdG9yKHBlcnNvLGFjdGlvbj0xKXtcbiAgICB0aGlzLndhdGVyRG1nPXNraWxsc2V0W3BlcnNvLnByb2Zlc3Npb25dW2FjdGlvbi0xXS53YXRlcixcbiAgICB0aGlzLmZpcmVEbWc9c2tpbGxzZXRbcGVyc28ucHJvZmVzc2lvbl1bYWN0aW9uLTFdLmZpcmUsXG4gICAgdGhpcy5lYXJ0aERtZz1za2lsbHNldFtwZXJzby5wcm9mZXNzaW9uXVthY3Rpb24tMV0uZWFydGgsXG4gICAgdGhpcy5zaGFkb3dEbWc9c2tpbGxzZXRbcGVyc28ucHJvZmVzc2lvbl1bYWN0aW9uLTFdLnNoYWRvdyxcbiAgICB0aGlzLnNraWxsTmFtZT1za2lsbHNldFtwZXJzby5wcm9mZXNzaW9uXVthY3Rpb24tMV0ubmFtZVxuICB9XG5cbiAgZGVhbFdhdGVyRG1nKGJvbnVzKXtcbiAgICBsZXQgd2F0ZXI9cGFyc2VJbnQodGhpcy53YXRlckRtZyk7XG4gICAgcmV0dXJuIHdhdGVyKmJvbnVzXG4gIH1cblxuICBkZWFsRmlyZURtZyhib251cyl7XG4gICAgbGV0IGZpcmU9cGFyc2VJbnQodGhpcy5maXJlRG1nKTtcbiAgICByZXR1cm4gZmlyZSpib251c1xuICB9XG5cbiAgZGVhbEVhcnRoRG1nKGJvbnVzKXtcbiAgIGxldCAgZWFydGg9cGFyc2VJbnQodGhpcy5lYXJ0aERtZyk7XG4gICAgcmV0dXJuIGVhcnRoKmJvbnVzXG4gIH1cblxuICBkZWFsU2hhZG93RG1nKGJvbnVzKXtcbiAgICBsZXQgc2hhZG93PXBhcnNlSW50KHRoaXMuc2hhZG93RG1nKTtcbiAgICByZXR1cm4gc2hhZG93KmJvbnVzXG4gIH1cblxuICBkZWFsQWxsRG1nKGJvbnVzKXtcbiAgICByZXR1cm4gdGhpcy5kZWFsV2F0ZXJEbWcoYm9udXMpK3RoaXMuZGVhbEVhcnRoRG1nKGJvbnVzKSt0aGlzLmRlYWxGaXJlRG1nKGJvbnVzKSt0aGlzLmRlYWxTaGFkb3dEbWcoYm9udXMpXG4gIH1cbn0iLCJpbXBvcnQgeyBDaGFyYWN0ZXIgfSBmcm9tICcuL2NoYXJhY3Rlcic7XG5pbXBvcnQgQ2hhcmFjdGVyQXR0cmlidXRzIGZyb20gJy4uL2pzb24vam9iQXR0cmlidXRzLmpzb24nXG5pbXBvcnQge1NraWxsfSBmcm9tICcuL3NraWxsJ1xuXG5leHBvcnQgY2xhc3MgVG9vbGJveCB7XG4gIGNvbnN0cnVjdG9yKCkge1xuXG4gIH1cbiAgY3JlYXRlQ2hhcmFjdGVyKG5hbWUsIHByb2Zlc3Npb24pIHtcblxuICAgIGlmIChwcm9mZXNzaW9uID09PSBcIldpemFyZFwiKSB7XG4gICAgICBsZXQgY2hhcmFjdGVyID0gbmV3IENoYXJhY3RlcihuYW1lLCBwcm9mZXNzaW9uLCAxNjAsIDIwMCwgMTAsIDEwLCAyMCk7XG4gICAgICByZXR1cm4gY2hhcmFjdGVyO1xuICAgIH1cbiAgICBlbHNlIGlmIChwcm9mZXNzaW9uID09PSBcIkZpZ2h0ZXJcIikge1xuICAgICAgbGV0IGNoYXJhY3RlciA9IG5ldyBDaGFyYWN0ZXIobmFtZSwgcHJvZmVzc2lvbiwgMjAwLCAxMDAsIDE4LCAxMiwgMTApO1xuICAgICAgcmV0dXJuIGNoYXJhY3RlcjtcbiAgICB9XG4gICAgZWxzZSBpZiAocHJvZmVzc2lvbiA9PT0gXCJUaGllZlwiKSB7XG4gICAgICBsZXQgY2hhcmFjdGVyID0gbmV3IENoYXJhY3RlcihuYW1lLCBwcm9mZXNzaW9uLCAxODAsIDE1MCwgMjAsIDEwLCAxMCk7XG4gICAgICByZXR1cm4gY2hhcmFjdGVyO1xuICAgIH1cbiAgICBlbHNlIGlmIChwcm9mZXNzaW9uID09PSBcIlBhbGFkaW5cIikge1xuICAgICAgbGV0IGNoYXJhY3RlciA9IG5ldyBDaGFyYWN0ZXIobmFtZSwgcHJvZmVzc2lvbiwgMTkwLCAxNDAsIDEzLCAxNSwgMTIpO1xuICAgICAgcmV0dXJuIGNoYXJhY3RlcjtcbiAgICB9XG4gIH1cblxuICAvKipcbiAqIFxuICogQHBhcmFtIHtzdHJpbmd9IGZyb20gdGhlIHNvdXJjZSBvZiB0aGUgZGFtYWdlXG4gKiBAcGFyYW0ge3N0cmluZ30gdG8gdGhlIGNoYXIgd291bmRlZFxuICogQHBhcmFtIHtudW1iZXJ9IGRtZyB0aGUgbnVtYmVyIG9mIGRhbWFnZVxuICovXG4gIGFkZExvZ0NvbWJhdChmcm9tLCB0bywgZG1nKSB7XG4gICAgbGV0IGxvZ0NvbWJhdCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3AnKTtcbiAgICBsb2dDb21iYXQudGV4dENvbnRlbnQgPSBgJHt0b30gdGFrZXMgJHtkbWd9IGRhbWFnZXMgZnJvbSAke2Zyb219YDtcblxuICAgIGxldCBsb2dTY3JvbGwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcjc2Nyb2xsJyk7XG4gICAgbG9nU2Nyb2xsLnByZXBlbmQobG9nQ29tYmF0KTtcblxuICAgIHJldHVybiBkbWc7XG4gIH1cblxuICAvKipcbiAqIFxuICogQHBhcmFtIHtDaGFyYWN0ZXJ9IGNoYXJhY3RlciBjaGFyYWN0ZXIgb2JqZWN0XG4gKi9cbiAgY29tcHV0ZUJvbnVzKGNoYXJhY3Rlcikge1xuXG4gICAgbGV0IHJhbmRvbWl6ZXIgPSBNYXRoLnJhbmRvbSgpICogKDEuNSAtIDEgKyAxKSArIDE7XG5cblxuICAgIGlmIChjaGFyYWN0ZXIucHJvZmVzc2lvbiA9PT0gXCJGaWdodGVyXCIpIHtcblxuICAgICAgbGV0IHNraWxsID0gTWF0aC5yb3VuZChjaGFyYWN0ZXIuc3RyZW5ndGggKiByYW5kb21pemVyKTtcbiAgICAgIHJldHVybiBza2lsbCAvIDEwO1xuICAgIH1cblxuICAgIGVsc2UgaWYgKGNoYXJhY3Rlci5wcm9mZXNzaW9uID09PSBcIldpemFyZFwiKSB7XG5cbiAgICAgIGxldCBza2lsbCA9IE1hdGgucm91bmQoY2hhcmFjdGVyLm1hZ2ljICogcmFuZG9taXplcik7XG4gICAgICByZXR1cm4gc2tpbGwgLyAxMDtcbiAgICB9XG5cbiAgICBlbHNlIGlmIChjaGFyYWN0ZXIucHJvZmVzc2lvbiA9PT0gXCJUaGllZlwiKSB7XG5cbiAgICAgIGxldCBza2lsbCA9IE1hdGgucm91bmQoY2hhcmFjdGVyLnN0cmVuZ3RoICsgNSAqIHJhbmRvbWl6ZXIpO1xuICAgICAgcmV0dXJuIHNraWxsIC8gMTA7XG4gICAgfVxuXG4gICAgZWxzZSBpZiAoY2hhcmFjdGVyLnByb2Zlc3Npb24gPT09IFwiUGFsYWRpblwiKSB7XG5cbiAgICAgIGNvbnNvbGUubG9nKGNoYXJhY3Rlci5zdHJlbmd0aCk7XG5cbiAgICAgIGxldCBza2lsbCA9IE1hdGgucm91bmQoKGNoYXJhY3Rlci5zdHJlbmd0aCArIGNoYXJhY3Rlci5tYWdpYykgLyAyICogcmFuZG9taXplcilcblxuICAgICAgcmV0dXJuIChza2lsbCAvIDEwKTtcbiAgICB9XG5cbiAgfVxuXG4gIC8qKlxuICogVmVyaWZpZXMgaWYgdGhlIGZpZ2h0ZXJzIGFyZSBhbGl2ZS4gV2hlbiBvbmUgZGllcywgaGlzIGFsaXZlIHByb3AgdHVybiB0byBmYWxzZVxuICogQHBhcmFtIHtDaGFyYWN0ZXJ9IHBsYXllciBcbiAqIEBwYXJhbSB7Q2hhcmFjdGVyfSBlbmVteSBcbiAqL1xuICBpc092ZXIoZW5lbXkpIHtcbiAgICBpZiAoZW5lbXkuaGVhbHRoIDw9IDApIHtcbiAgICAgIGVuZW15LmFsaXZlID0gZmFsc2U7XG4gICAgICBhbGVydChgJHtlbmVteS5uYW1lfSBpcyBkZWZlYXRlZC4gXFxuIENvbmdyYXR1bGF0aW9uIWApO1xuICAgICAgbG9jYXRpb24ucmVsb2FkKCk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIExhdW5jaGVzIGEgYmF0dGxlIHBoYXNlIGJldHdlZW4gdHdvIENoYXJhY3RlcnMuIFdoZW4gdGhlIHBsYXllciBkb2VzIGFuIGFjdGlvbiwgdGhlIGVuZW15IHJlcGxpZXMgYWZ0ZXIgMS4yIHNlY29uZHMuIFBsYXllciBjYW4ndCBwbGF5IGZvciAyIHNlY29uZHNcbiAgICogQHBhcmFtIHtDaGFyYWN0ZXJ9IHBsYXllciBcbiAgICogQHBhcmFtIHtDaGFyYWN0ZXJ9IGVuZW15IFxuICAgKiBAcGFyYW0ge051bWJlcn0gZGFtYWdlIFxuICAgKi9cbiAgY29tYmF0QWN0aW9uKHBsYXllciwgZW5lbXksIGRhbWFnZSA9IDAsIGhlYWwgPSBmYWxzZSkge1xuICAgIGxldCBjb21iYXRPdmVyID0gZmFsc2U7XG4gICAgLy9nZXN0aW9uIGJhcnJlIGRlIHZpZSBwbGF5ZXJcbiAgICBsZXQgcGxheWVySGVhbHRoQmFyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgI3BsYXllcjEgLmJsb2MtaGVhbHRoIC5oZWFsdGhgKTtcbiAgICAvL2dlc3Rpb24gYmFycmUgZGUgdmllIGVuZW15XG4gICAgbGV0IGVuZW15SGVhbHRoQmFyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihgI2VuZW15IC5ibG9jLWhlYWx0aCAuaGVhbHRoYCk7XG4gICAgaWYgKCFoZWFsKSB7XG4gICAgICBlbmVteS5oZWFsdGggLT0gZGFtYWdlO1xuICAgICAgZW5lbXkubWFuYWdlSGVhbHRoKCk7XG4gICAgICB0aGlzLmFkZExvZ0NvbWJhdChwbGF5ZXIubmFtZSwgZW5lbXkubmFtZSwgZGFtYWdlKVxuXG4gICAgICBpZiAoZW5lbXkuaGVhbHRoID4gMCkge1xuICAgICAgICBlbmVteUhlYWx0aEJhci50ZXh0Q29udGVudCA9IGAke01hdGguZmxvb3IoKGVuZW15LmhlYWx0aCAvIGVuZW15Lm1heEhlYWx0aCkgKiAxMDApfSVgXG4gICAgICAgIGVuZW15SGVhbHRoQmFyLnN0eWxlLndpZHRoID0gYCR7KGVuZW15LmhlYWx0aCAvIGVuZW15Lm1heEhlYWx0aCkgKiAxMDB9JWBcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICBlbmVteUhlYWx0aEJhci50ZXh0Q29udGVudCA9IGAwJWBcbiAgICAgICAgZW5lbXlIZWFsdGhCYXIuc3R5bGUud2lkdGggPSBgMCVgXG4gICAgICAgIHRoaXMuaXNPdmVyKGVuZW15KTtcbiAgICAgIH1cblxuICAgICAgY29uc29sZS5sb2coYGVuZW15IGhlYWx0aCAke2VuZW15LmhlYWx0aH0gYW5kICR7ZGFtYWdlfSBkbWdgKVxuICAgICAgaWYgKGVuZW15LmhlYWx0aCA+IDApIHtcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgbGV0IGVuZW15RGFtYWdlID0gTWF0aC5yb3VuZChlbmVteS5tYWdpYyAqIChNYXRoLnJhbmRvbSgpICogKDEuNSAtIDEgKyAxKSArIDEpKTtcbiAgICAgICAgICBwbGF5ZXIuaGVhbHRoIC09IGVuZW15RGFtYWdlIDtcblxuICAgICAgICAgIHBsYXllci5tYW5hZ2VIZWFsdGgoKTtcblxuICAgICAgICAgIGlmIChwbGF5ZXIuaGVhbHRoID4gMCkge1xuICAgICAgICAgICAgcGxheWVySGVhbHRoQmFyLnRleHRDb250ZW50ID0gYCR7TWF0aC5mbG9vcigocGxheWVyLmhlYWx0aCAvIHBsYXllci5tYXhIZWFsdGgpICogMTAwKX0lYFxuICAgICAgICAgICAgcGxheWVySGVhbHRoQmFyLnN0eWxlLndpZHRoID0gYCR7KHBsYXllci5oZWFsdGggLyBwbGF5ZXIubWF4SGVhbHRoKSAqIDEwMH0lYFxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHBsYXllckhlYWx0aEJhci50ZXh0Q29udGVudCA9IGAwJWBcbiAgICAgICAgICAgIHBsYXllckhlYWx0aEJhci5zdHlsZS53aWR0aCA9IGAwJWBcbiAgICAgICAgICAgIHRoaXMuZ2FtZU92ZXIoKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBjb25zb2xlLmxvZyhgUGxheWVyIGhlYWx0aCAke3BsYXllci5oZWFsdGh9IGFuZCAke2VuZW15Lm1hZ2ljfWApXG5cbiAgICAgICAgICB0aGlzLmFkZExvZ0NvbWJhdChlbmVteS5uYW1lLCBwbGF5ZXIubmFtZSwgZW5lbXlEYW1hZ2UpXG5cbiAgICAgICAgfSwgMTIwMCk7XG4gICAgICB9XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgcGxheWVyLmhlYWx0aCArPSAyMjtcbiAgICAgIHBsYXllci5tYW5hZ2VIZWFsdGgoKTtcbiAgICAgIC8vdGhpcy5hZGRMb2dDb21iYXQodGhpcy5hZGRMb2dDb21iYXQocGxheWVyLm5hbWUsIHBsYXllci5uYW1lLCAtMjIpKVxuXG4gICAgICBpZiAocGxheWVyLmhlYWx0aCA+IDApIHtcbiAgICAgICAgcGxheWVySGVhbHRoQmFyLnRleHRDb250ZW50ID0gYCR7TWF0aC5mbG9vcigocGxheWVyLmhlYWx0aCAvIHBsYXllci5tYXhIZWFsdGgpICogMTAwKX0lYFxuICAgICAgICBwbGF5ZXJIZWFsdGhCYXIuc3R5bGUud2lkdGggPSBgJHsocGxheWVyLmhlYWx0aCAvIHBsYXllci5tYXhIZWFsdGgpICogMTAwfSVgXG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgcGxheWVySGVhbHRoQmFyLnRleHRDb250ZW50ID0gYDAlYFxuICAgICAgICBwbGF5ZXJIZWFsdGhCYXIuc3R5bGUud2lkdGggPSBgMCVgXG4gICAgICAgIHRoaXMuZ2FtZU92ZXIoKTtcbiAgICAgIH1cblxuICAgICAgY29uc29sZS5sb2cocGxheWVyLmhlYWx0aCk7XG5cbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICBlbmVteS5oZWFsdGggKz0gMjI7XG4gICAgICAgIGVuZW15Lm1hbmFnZUhlYWx0aCgpO1xuICAgICAgICBjb25zb2xlLmxvZyhgZW5lbXkgaGVhbHRoICR7ZW5lbXkuaGVhbHRofWApXG5cbiAgICAgICAgaWYgKGVuZW15LmhlYWx0aCA+IDApIHtcbiAgICAgICAgICBlbmVteUhlYWx0aEJhci50ZXh0Q29udGVudCA9IGAke01hdGguZmxvb3IoKGVuZW15LmhlYWx0aCAvIGVuZW15Lm1heEhlYWx0aCkgKiAxMDApfSVgXG4gICAgICAgICAgZW5lbXlIZWFsdGhCYXIuc3R5bGUud2lkdGggPSBgJHsoZW5lbXkuaGVhbHRoIC8gZW5lbXkubWF4SGVhbHRoKSAqIDEwMH0lYFxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIGVuZW15SGVhbHRoQmFyLnRleHRDb250ZW50ID0gYDAlYFxuICAgICAgICAgIGVuZW15SGVhbHRoQmFyLnN0eWxlLndpZHRoID0gYDAlYFxuICAgICAgICAgIHRoaXMuaXNPdmVyKGVuZW15KTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuYWRkTG9nQ29tYmF0KHBsYXllci5uYW1lLCBlbmVteS5uYW1lLCBkYW1hZ2UpXG5cbiAgICAgIH0sIDEyMDApO1xuICAgIH1cblxuICB9XG5cbiAgZ2FtZU92ZXIoKSB7XG4gICAgaWYgKGNvbmZpcm0oJ1lvdSBhcmUgZGVhZC4gXFxuIENsaWNrIFxcXCJPa1xcXCIgdG8gcmVzdGFydCB3aXRoIHRoaXMgY2hhcmFjdGVyLlxcbiBDbGljayBcXFwiQW5udWxlclxcXCIgdG8gY3JlYXQgYSBuZXcgY2hhcmFjdGVyJykpIHtcbiAgICAgIGxvY2F0aW9uLnJlbG9hZCgpO1xuICAgIH1cbiAgICBlbHNlIHtcbiAgICAgIGxvY2F0aW9uLmhyZWYgPSBcImNyZWF0ZS1jaGFyYWN0ZXIuaHRtbFwiO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICogXG4gKiBAcGFyYW0ge0NoYXJhY3Rlcn0gcGxheWVyIFxuICogQHBhcmFtIHtib29sZWFufSBjYW5BdHRhY2sgXG4gKiBAcGFyYW0ge0NoYXJhY3Rlcn0gZW5lbXkgXG4gKiBAcGFyYW0ge251bWJlcn0gYnV0dG9uTnVtYmVyIFxuICovXG4gIGNvbWJhdFZpZXcocGxheWVyLCBjYW5BdHRhY2ssIGVuZW15LCBidXR0b25OdW1iZXIpIHtcbiAgICBpZiAoY2FuQXR0YWNrKSB7XG4gICAgICBsZXQgc2tpbGwgPSBuZXcgU2tpbGwocGxheWVyLCBidXR0b25OdW1iZXIpO1xuICAgICAgbGV0IGJvbnVzID0gdGhpcy5jb21wdXRlQm9udXMocGxheWVyKVxuICAgICAgbGV0IGRtZyA9IHNraWxsLmRlYWxBbGxEbWcoYm9udXMpO1xuICAgICAgXG5cbiAgICAgIGNvbnNvbGUubG9nKGBNYXggaGVhbHRoIHBsYXllciA6ICR7cGxheWVyLm1heEhlYWx0aH1gKVxuXG4gICAgICBjYW5BdHRhY2sgPSBmYWxzZTtcbiAgICAgIHRoaXMuY29tYmF0QWN0aW9uKHBsYXllciwgZW5lbXksIGRtZyk7XG4gICAgICBjb25zb2xlLmxvZyhkbWcpXG4gICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgY2FuQXR0YWNrID0gdHJ1ZTtcbiAgICAgIH0sIDEzMDApO1xuICAgICAgLy9jb25zb2xlLmxvZyhgIHBkdiBhY3R1ZWwgOiAke3BsYXllci5oZWFsdGh9IGRlICR7cGxheWVyLm5hbWV9YClcbiAgICAgIFxuXG4gICAgICAvL2NvbnNvbGUubG9nKGAgcGR2IGFjdHVlbCBwcmUgY291cDogJHtlbmVteS5oZWFsdGh9IGRlICR7ZW5lbXkubmFtZX1gKVxuICAgICAgZW5lbXkudGFrZURhbWFnZShkbWcpO1xuICAgICAgZW5lbXkubWFuYWdlSGVhbHRoKCk7XG5cbiAgICAgIC8vY29uc29sZS5sb2coYCBwZHYgYWN0dWVsIDogJHtlbmVteS5oZWFsdGh9IGRlICR7ZW5lbXkubmFtZX1gKVxuXG4gICAgICBcbiAgICB9O1xuICB9O1xufSJdLCJzb3VyY2VSb290IjoiIn0=