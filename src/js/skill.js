import skillset from '../json/skills.json' 
export class Skill {
  constructor(perso,action=1){
    this.waterDmg=skillset[perso.profession][action-1].water,
    this.fireDmg=skillset[perso.profession][action-1].fire,
    this.earthDmg=skillset[perso.profession][action-1].earth,
    this.shadowDmg=skillset[perso.profession][action-1].shadow,
    this.skillName=skillset[perso.profession][action-1].name
  }

  dealWaterDmg(bonus){
    let water=parseInt(this.waterDmg);
    return water*bonus
  }

  dealFireDmg(bonus){
    let fire=parseInt(this.fireDmg);
    return fire*bonus
  }

  dealEarthDmg(bonus){
   let  earth=parseInt(this.earthDmg);
    return earth*bonus
  }

  dealShadowDmg(bonus){
    let shadow=parseInt(this.shadowDmg);
    return shadow*bonus
  }

  dealAllDmg(bonus){
    return this.dealWaterDmg(bonus)+this.dealEarthDmg(bonus)+this.dealFireDmg(bonus)+this.dealShadowDmg(bonus)
  }
}