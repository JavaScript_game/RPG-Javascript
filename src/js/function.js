import { Character } from './character';

/**
 * Function to create a character, an instance from the class Character
 * It takes a string called "profession" as a parameter and assigns stat points to the instance. It then creates the character
 * @param {string} name 
 * @param {string} profession 
 */
export function createCharacter(name, profession) {
  let character = new Character("", "", 0, 0, 0, 0, 0);
  if (profession === "Wizard") {
    character.name = name;
    character.profession = profession;
    character.health = 100;
    character.mana = 200;
    character.strength = 10;
    character.vitality = 10;
    character.magic = 20;
    return character;
  }
  else if (profession === "Fighter") {
    character.name = name;
    character.profession = profession;
    character.health = 200;
    character.mana = 100;
    character.strength = 18;
    character.vitality = 12;
    character.magic = 10;
    return character;
  }
  else if (profession === "Thief") {
    character.name = name;
    character.profession = profession;
    character.health = 150;
    character.mana = 150;
    character.strength = 20;
    character.vitality = 10;
    character.magic = 10;
    return character;
  }
  else if (profession === "Paladin") {
    character.name = name;
    character.profession = profession;
    character.health = 160;
    character.mana = 140;
    character.strength = 13;
    character.vitality = 15;
    character.magic = 12;
    return character;
  }
}

export function whoStarts() {
  let initiative = Math.round(Math.random());
  let result;
  initiative ? result = true : result = false;
  return result;
}

/**
 * Verifies if the fighters are alive. When one dies, his alive prop turn to false
 * @param {Character} player 
 * @param {Character} monster 
 */
export function isOver(player, monster) {
  let result = false;
  if (monster.health <= 0) {
    monster.alive = false;
    result = true;
    return result;
  }
  else if (player.health <= 0) {
    player.alive = false
    result = true;
    return result;
  }
  return result;
}

/**
 * Launches a battle phase between two Characters. When the player does an action, the monster replies after 1.2 seconds. Player can't replay for 2 seconds
 * @param {Character} player 
 * @param {Character} monster 
 */
function combatAction(player, monster) {
  let combatOver = false;

  monster.health -= player.dealPhysicalDmg();
  console.log(`${monster.name} : ${monster.health}`);

  setTimeout(() => {
    player.health -= monster.dealMagicDmg();
    console.log(`${player.name} : ${player.health}`)

  }, 1200);

  combatOver = isOver(player, monster);
  if (combatOver) {
    alert('somebody is ded');
  }
}