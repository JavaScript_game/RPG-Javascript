import { Character } from './character';
import {Toolbox} from './toolBox';
import skillList from '../json/skills.json'

/**
 * Theses variables get the name and profession of the main character (played character).
 * The stats of the character are defined by his profession.
 */
let name = document.querySelector('#name');
let profession = document.querySelector('#profession');
let mainCharacter = new Character('Kiki', 'Fighter', 0, 0, 0, 0, 0); 

let tools = new Toolbox(); // Contains numerous function.

/**
 * Here I assign the different stats to a variable and initialise them to a type = number
 */
let health = 0;
let mana = 0;
let strength = 0;
let vitality = 0;
let magic = 0;

/**
 * I target the different stats to different paragraph to display them as the player choses a profession
 */
let healthValue = document.querySelector('#job-health');
let manaValue = document.querySelector('#job-mana');
let strengthValue = document.querySelector('#job-strength');
let vitalityValue = document.querySelector('#job-vitality');
let magicValue = document.querySelector('#job-magic');
let option = document.querySelector('#profession');
let form = document.querySelector('#character-form');

option.addEventListener('change', function () {

  mainCharacter = tools.createCharacter(name.value, 
  profession.options[profession.selectedIndex].value);

  healthValue.textContent = `Health: ${mainCharacter.health}`;
  manaValue.textContent = `Mana: ${mainCharacter.mana}`;
  strengthValue.textContent = `Strength: ${mainCharacter.strength}`;
  vitalityValue.textContent = `Vitality: ${mainCharacter.vitality}`;
  magicValue.textContent = `Magic: ${mainCharacter.magic}`;

  console.log(mainCharacter);
});

let start = document.querySelector('#start');
start.addEventListener('click', function (event) {
  let characterStorage = JSON.stringify(mainCharacter)
  localStorage.setItem("mainCharacter", characterStorage);
});